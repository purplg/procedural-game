use bevy::{
    prelude::*,
    window::{CursorGrabMode, PrimaryWindow},
};

use crate::input::InputEvent;

#[derive(Debug, Default, Clone, Eq, PartialEq, Hash, States)]
pub enum GameState {
    #[default]
    Idle,
    Controlling,
}

impl GameState {
    pub fn is_controlling(&self) -> bool {
        matches!(self, GameState::Controlling)
    }
}

pub struct GameStatePlugin;

impl Plugin for GameStatePlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<GameState>();
        app.add_systems(PreUpdate, input);
        app.add_systems(OnEnter(GameState::Controlling), hide_cursor);
        app.add_systems(OnExit(GameState::Controlling), show_cursor);
    }
}

fn input(mut input: EventReader<InputEvent>, mut gamestate: ResMut<NextState<GameState>>) {
    for input in input.read() {
        if let InputEvent::Grab(grab) = input {
            gamestate.set(if *grab {
                GameState::Controlling
            } else {
                GameState::Idle
            });
        }
    }
}

fn hide_cursor(mut window: Query<&mut Window, With<PrimaryWindow>>) {
    if let Ok(mut window) = window.get_single_mut() {
        window.cursor.grab_mode = CursorGrabMode::Confined;
        window.cursor.visible = false;
    }
}

fn show_cursor(mut window: Query<&mut Window, With<PrimaryWindow>>) {
    if let Ok(mut window) = window.get_single_mut() {
        window.cursor.grab_mode = CursorGrabMode::None;
        window.cursor.visible = true;
    }
}
