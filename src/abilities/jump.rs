use std::time::Duration;

use bevy::prelude::*;
use bevy_rapier3d::prelude::Velocity;

use super::{
    conditions::{AbilityCondition, Airborn, Condition, Cooldown},
    Ability,
};
use crate::input::InputEvent;

pub(super) struct JumpPlugin;

impl Plugin for JumpPlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<Ability<Jump>>()
            .register_type::<AbilityCondition<Cooldown>>()
            .register_type::<AbilityCondition<Airborn>>()
            .add_systems(Update, input_system);
    }
}

#[derive(Bundle)]
pub struct JumpBundle {
    jump: Ability<Jump>,
    cooldown: AbilityCondition<Cooldown>,
    airborn: AbilityCondition<Airborn>,
}

impl Default for JumpBundle {
    fn default() -> Self {
        Self {
            jump: Ability(Jump),
            cooldown: Cooldown::new(Duration::from_secs_f32(0.5)),
            airborn: Airborn::new(),
        }
    }
}

#[derive(Copy, Clone, Reflect)]
pub(super) struct Jump;

pub(super) fn input_system(
    mut query: Query<
        (
            &Parent,
            &mut AbilityCondition<Cooldown>,
            &AbilityCondition<Airborn>,
        ),
        With<Ability<Jump>>,
    >,
    mut velocity_query: Query<&mut Velocity>,
    mut input: EventReader<InputEvent>,
) {
    if input.read().any(|event| matches!(event, InputEvent::Jump)) {
        for (parent, mut cooldown, airborn) in query.iter_mut() {
            if cooldown.no() && airborn.no() {
                if let Ok(mut velocity) = velocity_query.get_mut(parent.get()) {
                    velocity.linvel.y = 10.0;
                    cooldown.start();
                }
            }
        }
    }
}
