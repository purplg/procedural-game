use std::time::Duration;

use bevy::{prelude::*, reflect::Reflect};

use super::{AbilityCondition, Condition};

#[derive(Reflect)]
pub(in crate::abilities) struct Cooldown {
    length: Duration,
    remaining: Duration,
}

impl Cooldown {
    pub(in crate::abilities) fn new(length: Duration) -> AbilityCondition<Self> {
        AbilityCondition(Self {
            length,
            remaining: Duration::ZERO,
        })
    }

    pub(in crate::abilities) fn start(&mut self) {
        self.remaining = self.length;
    }
}

impl Condition for Cooldown {
    fn yes(&self) -> bool {
        !self.remaining.is_zero()
    }

    fn reset(&mut self) {
        self.remaining = self.length;
    }
}

pub(super) fn update(mut query: Query<&mut AbilityCondition<Cooldown>>, time: Res<Time>) {
    for mut cooldown in query.iter_mut() {
        cooldown.0.remaining = cooldown
            .0
            .remaining
            .checked_sub(time.delta())
            .unwrap_or_default();
    }
}
