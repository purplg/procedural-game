use std::time::Duration;

use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

use super::{AbilityCondition, Condition};

#[derive(Reflect)]
pub(in crate::abilities) struct Airborn {
    time: Duration,
}

impl Airborn {
    pub(in crate::abilities) fn new() -> AbilityCondition<Self> {
        AbilityCondition(Self {
            time: Duration::ZERO,
        })
    }
}

impl Condition for Airborn {
    fn yes(&self) -> bool {
        !self.time.is_zero()
    }

    fn reset(&mut self) {
        self.time = Duration::ZERO;
    }
}

pub(super) fn update(mut query: Query<&mut AbilityCondition<Airborn>>, time: Res<Time>) {
    for mut airborn in query.iter_mut() {
        airborn.0.time += time.delta();
    }
}

pub(super) fn check(
    mut query: Query<(&Parent, &mut AbilityCondition<Airborn>)>,
    ctx: Res<RapierContext>,
    collider: Query<(&Transform, &Collider)>,
) {
    for (parent, mut airborn) in query.iter_mut() {
        if let Ok((transform, collider)) = collider.get(parent.get()) {
            if let Some(shape) = collider.as_capsule() {
                if ctx
                    .cast_ray_and_get_normal(
                        transform.translation,
                        Vec3::NEG_Y,
                        shape.height(),
                        true,
                        QueryFilter::only_fixed(),
                    )
                    .map(|(_, cast)| cast.normal.dot(Vec3::Y) > 0.9)
                    .unwrap_or_default()
                {
                    airborn.reset();
                }
            }
        }
    }
}
