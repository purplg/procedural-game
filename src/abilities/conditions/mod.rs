use bevy::prelude::*;

pub(super) mod airborn;
pub(super) use airborn::Airborn;
pub(super) mod cooldown;
pub(super) use cooldown::Cooldown;

pub(super) struct ConditionPlugin;

impl Plugin for ConditionPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, cooldown::update)
            .add_systems(Update, (airborn::update, airborn::check));
    }
}

#[derive(Component, Deref, DerefMut, Reflect)]
pub(super) struct AbilityCondition<C>(C)
where
    C: Condition;

pub trait Condition {
    fn reset(&mut self);

    fn yes(&self) -> bool;

    fn no(&self) -> bool {
        !self.yes()
    }
}
