mod conditions;

mod jump;

use bevy::prelude::*;
pub use jump::JumpBundle;

pub struct AbilityPlugin;

impl Plugin for AbilityPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(conditions::ConditionPlugin)
            .add_plugins(jump::JumpPlugin);
    }
}

#[derive(Component, Copy, Clone, Default, Reflect)]
pub struct Ability<A>(A);
