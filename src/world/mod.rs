pub mod area;
pub mod chunk;
pub mod generator;
pub mod position;

use crate::graphgen::mission::new_mission;
use bevy::{color::palettes, pbr::wireframe::Wireframe, prelude::*};
use bevy_rapier3d::prelude::*;

use self::{area::Area, position::GridPosition};
use crate::{
    assets::textures::{DynamicTextures, DynamicTexturesPlugin},
    rng::RngSource,
};

pub struct WorldPlugin;

impl Plugin for WorldPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(DynamicTexturesPlugin)
            .add_systems(Startup, startup);
        #[cfg(debug_assertions)]
        app.add_systems(Update, gizmos);
        app.add_systems(Update, tick);
    }
}

fn startup(mut commands: Commands, mut rng: ResMut<RngSource>) {
    let rng = &mut **rng;
    let graph = new_mission(rng, crate::graphgen::mission::all_rulesets());

    let scale = 2.0;
    let shape = generator::ShapeFeatures::new(rng, graph, UVec2::new(16, 16), 3, scale);
    commands.insert_resource(shape);

    commands.insert_resource(AmbientLight {
        brightness: 500.0,
        ..default()
    });
}

fn tick(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    dynamic_textures: Res<DynamicTextures>,
    keys: Res<ButtonInput<KeyCode>>,
    mut shape: ResMut<generator::ShapeFeatures>,
    areas: Query<Entity, With<Area>>,
    mut rng: ResMut<RngSource>,
) {
    if keys.just_pressed(KeyCode::Space) {
        for area in &areas {
            commands.entity(area).despawn_recursive();
        }

        let scale = 2.0;
        if let Some(area) = shape.tick(&mut **rng) {
            for (position, object_id) in area.object_iter() {
                let mut entity = commands.spawn_empty();
                let mesh = object_id.mesh(&dynamic_textures, scale);
                let commands = entity.insert((PbrBundle {
                    mesh: meshes.add(mesh),
                    material: dynamic_textures.material.clone(),
                    transform: Transform::from_xyz(
                        position.x as f32 * scale,
                        position.y as f32 * scale,
                        position.z as f32 * scale,
                    ),
                    ..default()
                },));
                object_id.insert(commands);
                info!("Spawned object: {position:?}");
            }

            for chunk in area.chunks.iter().filter_map(Option::as_ref) {
                let mesh = chunk.generate_mesh(&area, &dynamic_textures);
                let collider = chunk.generate_collider(&mesh);
                let transform = Transform::from_xyz(
                    (chunk.position.x() * area.chunk_size().x) as f32 * scale,
                    0.0,
                    (chunk.position.z() * area.chunk_size().y) as f32 * scale,
                );
                info!("spawned chunk: {:?}", transform.translation);
                commands.spawn((
                    PbrBundle {
                        mesh: meshes.add(mesh),
                        material: dynamic_textures.material.clone(),
                        transform,
                        ..default()
                    },
                    // Wireframe,
                    RigidBody::Fixed,
                    collider,
                ));
            }
            commands.spawn(area);
        }
    }
}

fn gizmos(mut gizmos: Gizmos, area: Query<&Area>) {
    if let Ok(area) = area.get_single() {
        let grid_width = area.chunk_width() as f32 * area.block_size();
        let grid_height = area.chunk_height() as f32 * area.block_size();
        let width = area.cols() as f32 * grid_width;
        let height = area.rows() as f32 * grid_height;
        for col in 1..area.cols() + 1 {
            gizmos.ray(
                Vec3::new(col as f32 * grid_width, 0.0, 0.0),
                Vec3::Z * height,
                palettes::basic::BLUE.with_alpha(0.05),
            );
        }
        for row in 1..area.rows() + 1 {
            gizmos.ray(
                Vec3::new(0.0, 0.0, row as f32 * grid_height),
                Vec3::X * width,
                palettes::basic::RED.with_alpha(0.05),
            );
        }
    }
}
