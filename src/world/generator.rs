use crate::graphgen::shape::LayoutGenerator;
use crate::graphgen::{graph::MissionNode, mission::MissionGraph, shape::new_layout};
use bevy::{prelude::*, utils::HashMap};
use grid::Grid;
use rand::RngCore;

use super::{
    area::Area,
    chunk::{builder::ChunkBuilder, Chunk},
    position::{BlockPosition, ChunkPosition, GridPosition},
};
use crate::assets::{blocks::BlockId, objects::ObjectId};

pub trait AreaGenerator {
    fn generate<R: RngCore + 'static>(self, rng: &mut R) -> Area;
}

#[derive(Resource)]
pub struct ShapeFeatures {
    pub graph: MissionGraph,
    builders: Grid<Option<ChunkBuilder>>,
    objects: HashMap<BlockPosition, ObjectId>,
    chunk_size: UVec2,
    height: u8,
    scale: f32,
    layout_gen: LayoutGenerator<MissionGraph>,
}

impl ShapeFeatures {
    pub fn new<R: RngCore>(
        rng: &mut R,
        graph: MissionGraph,
        chunk_size: UVec2,
        height: u8,
        scale: f32,
    ) -> Self {
        Self {
            objects: Default::default(),
            builders: Grid::new(10, 10),
            layout_gen: LayoutGenerator::new(rng, &graph),
            graph,
            chunk_size,
            height,
            scale,
        }
    }

    pub fn tick<R: RngCore + 'static>(&mut self, rng: &mut R) -> Option<Area> {
        if !self.layout_gen.tick(&self.graph) {
            info!("Done generating");
            return None;
        }

        info!("Tick");

        for (pos, room) in self
            .layout_gen
            .grid()
            .indexed_iter()
            .filter_map(|(pos, room)| room.as_ref().map(|room| (pos, room)))
            .map(|(pos, room)| {
                (
                    ChunkPosition::new(
                        pos.0 as u32,
                        0,
                        pos.1 as u32,
                        self.chunk_size.x,
                        self.chunk_size.y,
                    ),
                    room,
                )
            })
        {
            let mut builder = ChunkBuilder::new(pos, self.chunk_size, BlockId::Floor);
            builder.border(rng, BlockId::Wall);
            for direction in room.connections.iter().copied() {
                builder.passage(rng, direction);
            }

            match room.mission_node {
                MissionNode::Entrance => {
                    //   0123
                    // 5 ####
                    // 6 #---
                    // 7 ####
                    // 8 ---#
                    // 9 ####

                    // K
                    let mut x = 5; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7), pos.local(x, 0, 9)],
                    );

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7), pos.local(x, 0, 9)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );
                }
                MissionNode::Boss => {
                    builder.border(rng, BlockId::Directional);
                }
                MissionNode::Key => {
                    // K
                    let mut x = 5; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 6), pos.local(x, 0, 8)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );
                }
                MissionNode::KeyMulti => {
                    //   0123456789
                    // 5 #--#-#---#
                    // 6 #-#--##-##
                    // 7 ##---#-#-#
                    // 8 #-#--#---#
                    // 9 #--#-#---#

                    // K
                    let mut x = 3; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 6), pos.local(x, 0, 8)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );

                    // M
                    x += 2; // 5
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 6
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 6)]);

                    x += 1; // 7
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 8
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 6)]);

                    x += 1; // 9
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );
                }
                MissionNode::KeyFinal => {
                    //   0123456789
                    // 5 #--#-#####
                    // 6 #-#--#----
                    // 7 ##---###--
                    // 8 #-#--#----
                    // 9 #--#-#----

                    // K
                    let mut x = 3; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 6), pos.local(x, 0, 8)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );

                    // F
                    x += 2; // 5
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 6
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7)],
                    );

                    x += 1; // 7
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7)],
                    );

                    x += 1; // 8
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 5)]);

                    x += 1; // 9
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 5)]);
                }
                MissionNode::Goal => {
                    //   0123
                    // 5 ####
                    // 6 #---
                    // 7 #-##
                    // 8 #--#
                    // 9 ####

                    // K
                    let mut x = 5; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7), pos.local(x, 0, 9)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );
                }
                _ => {}
            }

            self.builders
                .get_mut(pos.row() as usize, pos.column() as usize)
                .map(|value| *value = Some(builder));
        }

        let rows = self.builders.rows();
        let v = self
            .builders
            .iter()
            .map(|builder| {
                builder.as_ref().map(|builder| {
                    let position = builder.position();
                    Chunk::new(builder.build(), position, self.scale, self.height)
                })
            })
            .collect::<Vec<Option<Chunk>>>();

        Some(Area::new(
            Grid::from_vec(v, rows),
            self.objects.clone(),
            self.chunk_size,
            self.scale,
        ))
    }
}

impl AreaGenerator for ShapeFeatures {
    fn generate<R: RngCore + 'static>(self, rng: &mut R) -> Area {
        let mut builders = self.builders;

        let shape = new_layout(rng, &self.graph);
        for (pos, room) in shape
            .indexed_iter()
            .filter_map(|(pos, room)| room.as_ref().map(|room| (pos, room)))
            .map(|(pos, room)| {
                (
                    ChunkPosition::new(
                        pos.0 as u32,
                        0,
                        pos.1 as u32,
                        self.chunk_size.x,
                        self.chunk_size.y,
                    ),
                    room,
                )
            })
        {
            let mut builder = ChunkBuilder::new(pos, self.chunk_size, BlockId::Floor);
            builder.border(rng, BlockId::Wall);
            for direction in room.connections.iter().copied() {
                builder.passage(rng, direction);
            }

            match room.mission_node {
                MissionNode::Entrance => {
                    //   0123
                    // 5 ####
                    // 6 #---
                    // 7 ####
                    // 8 ---#
                    // 9 ####

                    // K
                    let mut x = 5; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7), pos.local(x, 0, 9)],
                    );

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7), pos.local(x, 0, 9)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );
                }
                MissionNode::Boss => {
                    builder.border(rng, BlockId::Directional);
                }
                MissionNode::Key => {
                    // K
                    let mut x = 5; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 6), pos.local(x, 0, 8)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );
                }
                MissionNode::KeyMulti => {
                    //   0123456789
                    // 5 #--#-#---#
                    // 6 #-#--##-##
                    // 7 ##---#-#-#
                    // 8 #-#--#---#
                    // 9 #--#-#---#

                    // K
                    let mut x = 3; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 6), pos.local(x, 0, 8)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );

                    // M
                    x += 2; // 5
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 6
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 6)]);

                    x += 1; // 7
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 8
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 6)]);

                    x += 1; // 9
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );
                }
                MissionNode::KeyFinal => {
                    //   0123456789
                    // 5 #--#-#####
                    // 6 #-#--#----
                    // 7 ##---###--
                    // 8 #-#--#----
                    // 9 #--#-#----

                    // K
                    let mut x = 3; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 7)]);

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 6), pos.local(x, 0, 8)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );

                    // F
                    x += 2; // 5
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 6
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7)],
                    );

                    x += 1; // 7
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7)],
                    );

                    x += 1; // 8
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 5)]);

                    x += 1; // 9
                    builder.set_blocks(rng, BlockId::Hole, [pos.local(x, 0, 5)]);
                }
                MissionNode::Goal => {
                    //   0123
                    // 5 ####
                    // 6 #---
                    // 7 #-##
                    // 8 #--#
                    // 9 ####

                    // K
                    let mut x = 5; // 0
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 6),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );

                    x += 1; // 1
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 9)],
                    );

                    x += 1; // 2
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [pos.local(x, 0, 5), pos.local(x, 0, 7), pos.local(x, 0, 9)],
                    );

                    x += 1; // 3
                    builder.set_blocks(
                        rng,
                        BlockId::Hole,
                        [
                            pos.local(x, 0, 5),
                            pos.local(x, 0, 7),
                            pos.local(x, 0, 8),
                            pos.local(x, 0, 9),
                        ],
                    );
                }
                _ => {}
            }

            builders
                .get_mut(pos.row() as usize, pos.column() as usize)
                .map(|value| *value = Some(builder));
        }

        let rows = builders.rows();
        let v = builders
            .into_vec()
            .into_iter()
            .map(|builder| {
                builder.map(|builder| {
                    let position = builder.position();
                    Chunk::new(builder.build(), position, self.scale, self.height)
                })
            })
            .collect::<Vec<Option<Chunk>>>();

        Area::new(
            Grid::from_vec(v, rows),
            self.objects,
            self.chunk_size,
            self.scale,
        )
    }
}
