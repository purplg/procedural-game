use std::ops::Mul;

use bevy::math::Vec3;

use crate::assets::blocks::FaceData;

#[derive(Default)]
pub struct MeshData {
    pub positions: Vec<[f32; 3]>,
    pub normals: Vec<[f32; 3]>,
    pub uvs: Vec<[f32; 2]>,
    pub indices: Vec<u32>,
}

impl MeshData {
    pub fn push<T: Into<FaceData>>(&mut self, scale: Vec3, shape: T) {
        let offset = self.positions.len() as u32;
        let shape: FaceData = shape.into();
        for vertex in &shape.vertices {
            self.positions.push([
                vertex.position[0] * scale.x,
                vertex.position[1] * scale.y,
                vertex.position[2] * scale.z,
            ]);
            self.normals.push(vertex.normal);
            self.uvs.push(vertex.uv);
        }
        self.indices
            .append(&mut shape.indices.into_iter().map(|x| x + offset).collect());
    }
}

impl Mul<Vec3> for MeshData {
    type Output = MeshData;

    fn mul(mut self, pos: Vec3) -> Self::Output {
        self.positions.iter_mut().for_each(|positions| {
            positions[0] *= pos.x;
            positions[1] *= pos.y;
            positions[2] *= pos.z;
        });
        self
    }
}

impl Mul<f32> for MeshData {
    type Output = MeshData;

    fn mul(self, pos: f32) -> Self::Output {
        self * Vec3::splat(pos)
    }
}
