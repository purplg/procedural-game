use bevy::prelude::UVec2;
use noise::{NoiseFn, Worley as WorleyNoise};
use rand::{distributions::Standard, Rng, RngCore};

use crate::direction::Direction;
use crate::{
    assets::blocks::BlockId,
    world::position::{GridPosition, LocalPosition},
};

pub trait ChunkMod {
    type Output;

    fn apply<R>(self, rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static;
}

pub(super) struct Percent {
    percent: f32,
    block_id: BlockId,
}

impl Percent {
    pub(super) fn new(percent: f32, block_id: BlockId) -> Self {
        Self { percent, block_id }
    }
}

impl ChunkMod for Percent {
    type Output = Vec<BlockId>;

    fn apply<R>(self, rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        rng.sample_iter::<f32, Standard>(Standard)
            .take((size.x * size.y) as usize)
            .zip(grid.iter_mut())
            .for_each(|(r, block)| {
                if r < self.percent {
                    *block = self.block_id;
                }
            });
    }
}

pub(super) struct Noise {
    pub(super) threshold: f64,
    pub(super) block_id: BlockId,
}

impl ChunkMod for Noise {
    type Output = Vec<BlockId>;

    fn apply<R>(self, rng: &mut R, _size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        let noise = WorleyNoise::new(rng.next_u32());
        grid.iter_mut()
            .enumerate()
            .map(|(i, block)| (noise.get([i as f64 * 0.5, i as f64 * 0.5]), block))
            .for_each(|(chance, block)| {
                if chance > self.threshold {
                    *block = self.block_id;
                };
            });
    }
}

pub(super) struct Filled(pub(super) BlockId);

impl ChunkMod for Filled {
    type Output = Vec<BlockId>;

    fn apply<R>(self, _rng: &mut R, _size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        for block in grid.iter_mut() {
            *block = self.0;
        }
    }
}

pub(super) struct Border(pub(super) BlockId);

impl ChunkMod for Border {
    type Output = Vec<BlockId>;

    fn apply<R>(self, _rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        for (x, y, block) in grid
            .iter_mut()
            .enumerate()
            .map(|(i, block)| (i as u32 % size.x, i as u32 / size.x, block))
        {
            if x == 0 || x == size.x - 1 || y == 0 || y == size.y - 1 {
                *block = self.0;
            }
        }
    }
}

pub(super) struct Center(pub(super) BlockId);

impl ChunkMod for Center {
    type Output = Vec<BlockId>;

    fn apply<R>(self, _rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        let x = size.x / 2;
        let y = size.y / 2;
        let i = y * size.x + x;
        if let Some(block) = grid.get_mut(i as usize) {
            *block = self.0;
        }
    }
}

pub(super) struct VLine {
    pub(super) offset: u32,
    pub(super) thickness: u32,
    pub(super) block_id: BlockId,
}

impl ChunkMod for VLine {
    type Output = Vec<BlockId>;

    fn apply<R>(self, _rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        for (x, block) in grid
            .iter_mut()
            .enumerate()
            .map(|(i, block)| (i as u32 % size.x, block))
        {
            if x >= self.offset && x < self.offset + self.thickness {
                *block = self.block_id;
            };
        }
    }
}

pub(super) struct HLine {
    pub(super) offset: u32,
    pub(super) thickness: u32,
    pub(super) block_id: BlockId,
}

impl ChunkMod for HLine {
    type Output = Vec<BlockId>;

    fn apply<R>(self, _rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        for (y, block) in grid
            .iter_mut()
            .enumerate()
            .map(|(i, block)| (i as u32 / size.x, block))
        {
            if y >= self.offset && y < self.offset + self.thickness {
                *block = self.block_id;
            }
        }
    }
}

pub(super) struct Door(pub(super) Direction);

impl ChunkMod for Door {
    type Output = Vec<BlockId>;

    fn apply<R>(self, _rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        let (x, y) = match self.0 {
            Direction::North => (size.x / 2, 0),
            Direction::East => (size.x - 1, size.y / 2),
            Direction::South => (size.x / 2, size.y - 1),
            Direction::West => (0, size.y / 2),
        };
        let i = y * size.x + x;
        if let Some(block) = grid.get_mut(i as usize) {
            *block = BlockId::Door;
        }
    }
}

pub(super) struct SetBlock {
    pub(super) block_id: BlockId,
    pub(super) position: LocalPosition,
}

impl ChunkMod for SetBlock {
    type Output = Vec<BlockId>;

    fn apply<R>(self, _rng: &mut R, size: UVec2, grid: &mut Vec<BlockId>)
    where
        R: RngCore + 'static,
    {
        if let Some(block) = grid.get_mut((self.position.z() * size.x + self.position.x()) as usize)
        {
            *block = self.block_id;
        }
    }
}
