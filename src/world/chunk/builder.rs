use std::ops::{Deref, DerefMut};

use bevy::prelude::UVec2;
use rand::RngCore;

use super::generation::{Border, Center, ChunkMod, Filled, SetBlock};
use crate::direction::Direction;
use crate::{
    assets::blocks::BlockId,
    world::position::{ChunkPosition, LocalPosition},
};

pub struct WithRng<'a, R: 'static, T> {
    rng: &'a mut R,
    inner: &'a mut T,
}

impl<'a, R, T> Deref for WithRng<'a, R, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<'a, R, T> DerefMut for WithRng<'a, R, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

pub struct ChunkBuilder {
    position: ChunkPosition,
    size: UVec2,
    chunk: Vec<BlockId>,
}

impl ChunkBuilder {
    pub fn new(position: ChunkPosition, size: UVec2, start_with: BlockId) -> Self {
        let len = size.x as usize * size.y as usize;
        let mut chunk = Vec::with_capacity(len);
        chunk.extend(std::iter::repeat(start_with).take(len));
        Self {
            position,
            chunk,
            size,
        }
    }

    pub fn position(&self) -> ChunkPosition {
        self.position
    }

    /// Begin a series of chunk modifications requiring an RNG source.
    ///
    /// This just removes the requirement to constantly pass in an RNG reference for more
    /// ergonomics.
    pub fn with_rng<'a, R>(&'a mut self, rng: &'a mut R) -> WithRng<'a, R, Self> {
        WithRng { rng, inner: self }
    }

    pub fn fill<'a, R>(&'a mut self, rng: &mut R, block: BlockId) -> &mut Self
    where
        R: RngCore + 'static,
    {
        self.with_rng(rng).fill(block);
        self
    }

    pub fn border<R>(&mut self, rng: &mut R, block: BlockId) -> &mut Self
    where
        R: RngCore + 'static,
    {
        self.with_rng(rng).border(block);
        self
    }

    pub fn center<R>(&mut self, rng: &mut R, block: BlockId) -> &mut Self
    where
        R: RngCore + 'static,
    {
        self.with_rng(rng).center(block);
        self
    }

    pub fn set_block<R>(
        &mut self,
        rng: &mut R,
        block_id: BlockId,
        position: LocalPosition,
    ) -> &mut Self
    where
        R: RngCore + 'static,
    {
        self.with_rng(rng).set_block(block_id, position);
        self
    }

    pub fn set_blocks<R>(
        &mut self,
        rng: &mut R,
        block_id: BlockId,
        positions: impl IntoIterator<Item = LocalPosition>,
    ) -> &mut Self
    where
        R: RngCore + 'static,
    {
        for position in positions.into_iter() {
            self.with_rng(rng).set_block(block_id, position);
        }
        self
    }

    pub fn passage<R>(&mut self, rng: &mut R, direction: Direction) -> &mut Self
    where
        R: RngCore + 'static,
    {
        self.with_rng(rng).passage(direction);
        self
    }

    pub fn build(&self) -> Vec<BlockId> {
        self.chunk.clone()
    }
}

impl<'a, R> WithRng<'a, R, ChunkBuilder>
where
    R: RngCore + 'static,
{
    pub fn apply<G: ChunkMod>(self, generator: G) -> Self {
        generator.apply(self.rng, self.inner.size, &mut self.inner.chunk);
        self
    }

    pub fn fill(self, block: BlockId) -> Self {
        self.apply(Filled(block))
    }

    pub fn border(self, block: BlockId) -> Self {
        self.apply(Border(block))
    }

    pub fn center(self, block: BlockId) -> Self {
        self.apply(Center(block))
    }

    pub fn set_block(self, block_id: BlockId, position: LocalPosition) -> Self {
        self.apply(SetBlock { block_id, position })
    }

    pub fn passage(self, direction: Direction) -> Self {
        let (x, y) = match direction {
            Direction::North => (self.size.x / 2, 0),
            Direction::East => (self.size.x - 1, self.size.y / 2),
            Direction::South => (self.size.x / 2, self.size.y - 1),
            Direction::West => (0, self.size.y / 2),
        };
        let position = self.position.local(x, 0, y);
        self.apply(SetBlock {
            block_id: BlockId::Floor,
            position,
        })
    }
}
