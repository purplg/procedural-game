pub mod builder;
pub mod generation;
pub mod mesh;

use bevy::{
    prelude::*,
    render::{mesh::Indices, render_asset::RenderAssetUsages, render_resource::PrimitiveTopology},
};
use bevy_rapier3d::prelude::{Collider, ComputedColliderShape};

use self::mesh::MeshData;
use super::{
    position::{BlockPosition, ChunkPosition, GridPosition, LocalPosition},
    Area,
};
use crate::assets::{
    blocks::{BlockId, Facing},
    textures::DynamicTextures,
};

#[derive(Component, Debug)]
pub struct Chunk {
    pub grid: Vec<BlockId>,
    pub position: ChunkPosition,
    pub block_size: f32,
    pub height: u8,
}

impl Chunk {
    pub fn new(grid: Vec<BlockId>, position: ChunkPosition, block_size: f32, height: u8) -> Self {
        Self {
            grid,
            position,
            block_size,
            height,
        }
    }

    pub fn block(&self, position: &LocalPosition) -> Option<BlockId> {
        self.grid.get(position.index() as usize).copied()
    }

    pub fn generate_mesh(&self, area: &Area, world_mat: &DynamicTextures) -> Mesh {
        let mut mesh_data = MeshData::default();
        for (local_pos, block) in self.local_iter() {
            let block_pos = self.position + local_pos;
            let scale = Vec3::ONE * self.block_size;
            if block_pos
                .north()
                .and_then(|pos| area.block(&pos))
                .map(BlockId::is_transparent)
                .unwrap_or_default()
            {
                for i in 0..self.height {
                    if let Some(face_data) = block.face(Facing::North, world_mat, i) {
                        mesh_data.push(scale, face_data + local_pos + Vec3::Y * i as f32);
                    }
                }
            }

            if block_pos
                .east()
                .and_then(|pos| area.block(&pos))
                .map(BlockId::is_transparent)
                .unwrap_or_default()
            {
                for i in 0..self.height {
                    if let Some(face_data) = block.face(Facing::East, world_mat, i) {
                        mesh_data.push(scale, face_data + local_pos + Vec3::Y * i as f32);
                    }
                }
            }

            if block_pos
                .south()
                .and_then(|pos| area.block(&pos))
                .map(BlockId::is_transparent)
                .unwrap_or_default()
            {
                for i in 0..self.height {
                    if let Some(face_data) = block.face(Facing::South, world_mat, i) {
                        mesh_data.push(scale, face_data + local_pos + Vec3::Y * i as f32);
                    }
                }
            }

            if block_pos
                .west()
                .and_then(|pos| area.block(&pos))
                .map(BlockId::is_transparent)
                .unwrap_or_default()
            {
                for i in 0..self.height {
                    if let Some(face_data) = block.face(Facing::West, world_mat, i) {
                        mesh_data.push(scale, face_data + local_pos + Vec3::Y * i as f32);
                    }
                }
            }

            if let Some(face_data) = block.face(Facing::Up, world_mat, self.height) {
                mesh_data.push(scale, face_data + local_pos);
            }

            if let Some(face_data) = block.face(Facing::Down, world_mat, self.height) {
                mesh_data.push(scale, face_data + local_pos);
            }
        }
        let mut mesh = Mesh::new(PrimitiveTopology::TriangleList, RenderAssetUsages::all());
        mesh.insert_indices(Indices::U32(mesh_data.indices.clone()));
        mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, mesh_data.positions);
        mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, mesh_data.normals);
        mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, mesh_data.uvs);
        mesh
    }

    pub fn generate_collider(&self, mesh: &Mesh) -> Collider {
        Collider::from_bevy_mesh(mesh, &ComputedColliderShape::TriMesh).unwrap()
    }

    pub fn local_iter(&self) -> LocalIter<'_> {
        LocalIter::new(self)
    }

    pub fn block_iter(&self) -> BlockIter<'_> {
        BlockIter::new(self.local_iter())
    }
}

pub struct LocalIter<'a> {
    chunk: &'a Chunk,
    index: u32,
}

impl<'a> LocalIter<'a> {
    fn new(chunk: &'a Chunk) -> Self {
        Self { chunk, index: 0 }
    }
}

impl<'a> Iterator for LocalIter<'a> {
    type Item = (LocalPosition, BlockId);

    fn next(&mut self) -> Option<Self::Item> {
        let position = self.chunk.position.local_from_index(self.index);
        self.index += 1;
        self.chunk
            .block(&position)
            .map(|block_id| (position, block_id))
    }
}

pub struct BlockIter<'a> {
    inner: LocalIter<'a>,
}

impl<'a> BlockIter<'a> {
    fn new(inner: LocalIter<'a>) -> Self {
        Self { inner }
    }
}

impl<'a> Iterator for BlockIter<'a> {
    type Item = (BlockPosition, BlockId);

    fn next(&mut self) -> Option<Self::Item> {
        self.inner
            .next()
            .map(|(local_pos, block_id)| (self.inner.chunk.position + local_pos, block_id))
    }
}
