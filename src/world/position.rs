use std::ops::{Add, Sub};

use bevy::prelude::*;

use crate::direction::Direction;
use crate::world::Area;

#[derive(Hash, PartialEq, Eq, Component, Clone, Copy, Deref, Debug, Reflect)]
pub struct BlockPosition(UVec3);

impl BlockPosition {
    pub fn new(x: u32, y: u32, z: u32) -> Self {
        Self(UVec3::new(x, y, z))
    }

    pub fn new_from_index(i: u32, cols: u32) -> Self {
        Self::new(i % cols, 0, i / cols)
    }

    pub fn chunk(&self, area: &Area) -> ChunkPosition {
        ChunkPosition::new(
            self.x / area.chunk_width(),
            self.y,
            self.z / area.chunk_height(),
            area.chunk_width(),
            area.chunk_height(),
        )
    }

    pub fn local(&self, area: &Area) -> LocalPosition {
        LocalPosition::new(
            self.x % area.chunk_width(),
            self.y,
            self.z % area.chunk_height(),
            self.chunk(area),
        )
    }

    pub fn split(&self, area: &Area) -> (ChunkPosition, LocalPosition) {
        (self.chunk(area), self.local(area))
    }
}

impl GridPosition for BlockPosition {
    type Owned = BlockPosition;

    fn x(&self) -> u32 {
        self.0.x
    }

    fn y(&self) -> u32 {
        self.0.y
    }

    fn z(&self) -> u32 {
        self.0.z
    }

    fn with(&self, x: u32, y: u32, z: u32) -> Self {
        Self(UVec3::new(x, y, z))
    }
}

impl Add<Vec3> for BlockPosition {
    type Output = Vec3;

    fn add(self, rhs: Vec3) -> Self::Output {
        Vec3::add(self.as_vec3(), rhs)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct ChunkPosition {
    pos: UVec3,
    pub width: u32,
    pub height: u32,
}

impl ChunkPosition {
    pub fn new(x: u32, y: u32, z: u32, width: u32, height: u32) -> Self {
        Self {
            pos: UVec3::new(x, y, z),
            width,
            height,
        }
    }

    pub fn new_from_index(i: u32, cols: u32, width: u32, height: u32) -> Self {
        Self::new(i % cols, 0, i / cols, width, height)
    }

    pub fn size(&self) -> UVec2 {
        UVec2::new(self.width, self.height)
    }

    pub fn index(&self, cols: u32) -> u32 {
        self.z() * cols + self.x()
    }

    pub fn local(&self, x: u32, y: u32, z: u32) -> LocalPosition {
        LocalPosition::new(x, y, z, *self)
    }

    pub fn local_from_index(&self, i: u32) -> LocalPosition {
        self.local(i % self.width, 0, i / self.width)
    }
}

impl GridPosition for ChunkPosition {
    type Owned = ChunkPosition;

    fn x(&self) -> u32 {
        self.pos.x
    }

    fn y(&self) -> u32 {
        self.pos.y
    }

    fn z(&self) -> u32 {
        self.pos.z
    }

    fn with(&self, x: u32, y: u32, z: u32) -> Self {
        Self::new(x, y, z, self.width, self.height)
    }
}

impl Add<LocalPosition> for ChunkPosition {
    type Output = BlockPosition;

    fn add(self, rhs: LocalPosition) -> Self::Output {
        BlockPosition::new(
            self.x() * self.width + rhs.x(),
            self.y() * self.height + rhs.y(),
            self.z() * self.height + rhs.z(),
        )
    }
}

impl Sub<LocalPosition> for ChunkPosition {
    type Output = BlockPosition;

    fn sub(self, rhs: LocalPosition) -> Self::Output {
        BlockPosition::new(
            self.x() * self.width - rhs.x(),
            self.y() * self.height - rhs.y(),
            self.z() * self.height - rhs.z(),
        )
    }
}

impl Add<UVec3> for ChunkPosition {
    type Output = BlockPosition;

    fn add(self, rhs: UVec3) -> Self::Output {
        BlockPosition::new(
            self.x() * self.width + rhs.x,
            self.y() * self.height + rhs.y,
            self.z() * self.height + rhs.z,
        )
    }
}

impl Sub<UVec3> for ChunkPosition {
    type Output = BlockPosition;

    fn sub(self, rhs: UVec3) -> Self::Output {
        BlockPosition::new(
            self.x() * self.width - rhs.x,
            self.y() * self.height - rhs.y,
            self.z() * self.height - rhs.z,
        )
    }
}

impl Add<Vec3> for ChunkPosition {
    type Output = Vec3;

    fn add(self, rhs: Vec3) -> Self::Output {
        Vec3::add(self.as_vec3(), rhs)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct LocalPosition {
    pos: UVec3,
    parent: ChunkPosition,
}

impl LocalPosition {
    fn new(x: u32, y: u32, z: u32, parent: ChunkPosition) -> Self {
        Self {
            pos: UVec3::new(x, y, z),
            parent,
        }
    }

    pub fn index(&self) -> u32 {
        self.z() * self.parent.width + self.x()
    }
}

impl GridPosition for LocalPosition {
    type Owned = LocalPosition;

    fn x(&self) -> u32 {
        self.pos.x
    }

    fn y(&self) -> u32 {
        self.pos.y
    }

    fn z(&self) -> u32 {
        self.pos.z
    }

    fn with(&self, x: u32, y: u32, z: u32) -> Self {
        LocalPosition::new(x, y, z, self.parent)
    }
}

impl GridPosition for &LocalPosition
where
    Self: Clone,
{
    type Owned = LocalPosition;

    fn x(&self) -> u32 {
        self.pos.x
    }

    fn y(&self) -> u32 {
        self.pos.y
    }

    fn z(&self) -> u32 {
        self.pos.z
    }

    fn with(&self, x: u32, y: u32, z: u32) -> Self::Owned {
        LocalPosition::new(x, y, z, self.parent)
    }
}

impl Add<Vec3> for LocalPosition {
    type Output = Vec3;

    fn add(self, rhs: Vec3) -> Self::Output {
        Vec3::add(self.as_vec3(), rhs)
    }
}

pub trait GridPosition
where
    Self: Copy + Sized,
{
    type Owned;

    fn x(&self) -> u32;

    fn y(&self) -> u32;

    fn z(&self) -> u32;

    fn with(&self, x: u32, y: u32, z: u32) -> Self::Owned;

    fn with_x(&self, x: u32) -> Self::Owned {
        self.with(x, self.y(), self.z())
    }

    fn with_y(&self, y: u32) -> Self::Owned {
        self.with(self.x(), y, self.z())
    }

    fn with_z(&self, z: u32) -> Self::Owned {
        self.with(self.x(), self.y(), z)
    }

    fn row(&self) -> u32 {
        self.z()
    }

    fn column(&self) -> u32 {
        self.x()
    }

    fn step(&self, direction: Direction) -> Option<Self::Owned> {
        match direction {
            Direction::North => self.north(),
            Direction::East => self.east(),
            Direction::South => self.south(),
            Direction::West => self.west(),
        }
    }

    fn step_unchecked(&self, direction: Direction) -> Self::Owned {
        match direction {
            Direction::North => self.north_unchecked(),
            Direction::East => self.east_unchecked(),
            Direction::South => self.south_unchecked(),
            Direction::West => self.west_unchecked(),
        }
    }

    fn north(&self) -> Option<Self::Owned> {
        (self.z() > 0).then(|| self.north_unchecked())
    }

    fn east(&self) -> Option<Self::Owned> {
        Some(self.east_unchecked())
    }

    fn south(&self) -> Option<Self::Owned> {
        Some(self.south_unchecked())
    }

    fn west(&self) -> Option<Self::Owned> {
        (self.x() > 0).then(|| self.west_unchecked())
    }

    fn north_unchecked(&self) -> Self::Owned {
        self.with_z(self.z() - 1)
    }

    fn east_unchecked(&self) -> Self::Owned {
        self.with_x(self.x() + 1)
    }

    fn south_unchecked(&self) -> Self::Owned {
        self.with_z(self.z() + 1)
    }

    fn west_unchecked(&self) -> Self::Owned {
        self.with_x(self.x() - 1)
    }

    fn as_vec2(&self) -> Vec2 {
        Vec2::new(self.x() as f32, self.z() as f32)
    }

    fn as_vec3(&self) -> Vec3 {
        Vec3::new(self.x() as f32, self.y() as f32, self.z() as f32)
    }
}

impl GridPosition for (u32, u32) {
    type Owned = (u32, u32);

    fn x(&self) -> u32 {
        self.0
    }

    fn y(&self) -> u32 {
        0
    }

    fn z(&self) -> u32 {
        self.1
    }

    fn with(&self, x: u32, y: u32, z: u32) -> Self::Owned {
        (x, z)
    }
}
