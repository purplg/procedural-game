use bevy::{
    prelude::*,
    utils::{hashbrown::hash_map, HashMap},
};
use grid::Grid;

use super::{
    chunk::Chunk,
    position::{BlockPosition, ChunkPosition, GridPosition},
};
use crate::assets::{blocks::BlockId, objects::ObjectId};

/// A collection of generated chunks and objects.
#[derive(Component)]
pub struct Area {
    /// Chunks are stored in a flat 'row-major' Vec where the each row is contiguous.
    pub chunks: Grid<Option<Chunk>>,
    /// Positions where objects should spawn
    objects: HashMap<BlockPosition, ObjectId>,
    /// The size of a single chunk
    chunk_size: UVec2,
    /// A scalar to adjust the size of the generated mesh
    block_size: f32,
}

impl Area {
    pub fn new(
        chunks: Grid<Option<Chunk>>,
        objects: HashMap<BlockPosition, ObjectId>,
        chunk_size: UVec2,
        block_size: f32,
    ) -> Self {
        Self {
            chunks,
            objects,
            chunk_size,
            block_size,
        }
    }

    pub fn block(&self, position: &BlockPosition) -> Option<BlockId> {
        let (chunk_pos, local) = position.split(self);
        self.chunk(&chunk_pos).and_then(|chunk| chunk.block(&local))
    }

    pub fn object_at(&self, position: &BlockPosition) -> Option<&ObjectId> {
        self.objects.get(position)
    }

    pub fn chunk(&self, position: &ChunkPosition) -> Option<&Chunk> {
        self.chunks
            .get(position.row() as usize, position.column() as usize)
            .map(|inner| inner.as_ref())
            .unwrap_or_default()
    }

    pub fn block_size(&self) -> f32 {
        self.block_size
    }

    pub fn chunk_size(&self) -> UVec2 {
        self.chunk_size
    }

    pub fn chunk_width(&self) -> u32 {
        self.chunk_size.x
    }

    pub fn chunk_height(&self) -> u32 {
        self.chunk_size.y
    }

    pub fn cols(&self) -> u32 {
        self.chunks.cols() as u32
    }

    pub fn rows(&self) -> u32 {
        self.chunks.rows() as u32
    }

    pub fn block_iter(&self) -> Iter<'_> {
        Iter::new(self)
    }

    pub fn object_iter(&self) -> hash_map::Iter<'_, BlockPosition, ObjectId> {
        self.objects.iter()
    }
}

pub struct Iter<'a> {
    world: &'a Area,
    index: u32,
}

impl<'a> Iter<'a> {
    fn new(world: &'a Area) -> Self {
        Self { world, index: 0 }
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = (BlockPosition, BlockId);

    fn next(&mut self) -> Option<Self::Item> {
        let position = BlockPosition::new_from_index(self.index, self.world.cols());
        self.index += 1;
        self.world.block(&position).map(|a| (position, a))
    }
}
