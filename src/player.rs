use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

use crate::{abilities::JumpBundle, gamestate::GameState, input::InputEvent};

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ClearColor(Color::rgb(0.1, 0.0, 0.15)))
            .add_systems(Startup, setup)
            .add_systems(
                PreUpdate,
                (input_sprint, input_movement, input_camera)
                    .chain()
                    .run_if(|gamestate: Res<State<GameState>>| gamestate.is_controlling()),
            );
    }
}

fn setup(mut commands: Commands) {
    let start_pos = Transform {
        translation: Vec3::new(5.0, 1.5, 5.0),
        rotation: Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, 0.0),
        scale: Vec3::ONE,
    };
    commands
        .spawn((
            RigidBody::Dynamic,
            LockedAxes::ROTATION_LOCKED,
            Damping {
                linear_damping: 1.0,
                angular_damping: 0.0,
            },
            GravityScale(1.0),
            Player::new(50.0, 5.0, 2.0),
            Sprint::new(2.0),
            Velocity::default(),
            Sleeping::disabled(),
            Collider::capsule_y(1.0, 0.5),
            TransformBundle::from_transform(start_pos),
            ActiveEvents::CONTACT_FORCE_EVENTS,
        ))
        .with_children(|builder| {
            builder.spawn(JumpBundle::default());
        })
        .with_children(|builder| {
            builder.spawn(Camera3dBundle::default());
        });
}

#[derive(Component)]
struct Player {
    move_acceleration: f32,
    move_speed: f32,
    look_speed: f32,
}

impl Player {
    fn new(move_acceleration: f32, move_speed: f32, look_speed: f32) -> Self {
        Self {
            move_acceleration,
            move_speed,
            look_speed,
        }
    }
}

#[derive(Component)]
struct Sprint {
    sprinting: bool,
    multiplier: f32,
}

impl Sprint {
    pub fn new(multiplier: f32) -> Self {
        Self {
            sprinting: false,
            multiplier,
        }
    }

    fn speed(&self, base_speed: f32) -> f32 {
        if self.sprinting {
            base_speed * self.multiplier
        } else {
            base_speed
        }
    }
}

fn input_sprint(mut query: Query<&mut Sprint>, mut input: EventReader<InputEvent>, _dt: Res<Time>) {
    for mut sprint in query.iter_mut() {
        for input_event in input.read() {
            if let InputEvent::Sprint(value) = input_event {
                sprint.sprinting = *value;
            }
        }
    }
}

fn input_movement(
    mut query_player: Query<(&mut Velocity, &Player, Option<&Sprint>)>,
    query_camera: Query<&Transform, With<Camera3d>>,
    mut input: EventReader<InputEvent>,
    dt: Res<Time>,
) {
    if let Ok(camera_transform) = query_camera.get_single() {
        for (mut velocity, player, sprint) in query_player.iter_mut() {
            // Collect all movement inputs
            let movement = input.read().fold(Vec2::ZERO, |movement, event| {
                if let InputEvent::Move(input) = event {
                    movement + *input
                } else {
                    movement
                }
            });

            let direction =
                (camera_transform.forward() * movement.x) + (camera_transform.right() * movement.y);
            let direction = Vec2::new(direction.x, direction.z);

            // Apply movement inputs to player translation
            let vel = Vec2::new(velocity.linvel.x, velocity.linvel.z)
                + direction * player.move_acceleration * dt.delta_seconds();

            let vel = vel.clamp_length_max(
                sprint.map_or(player.move_speed, |sprint| sprint.speed(player.move_speed)),
            );
            velocity.linvel.x = vel.x;
            velocity.linvel.z = vel.y;
        }
    }
}

fn input_camera(
    mut query_camera: Query<(&mut Transform, &Parent), With<Camera3d>>,
    query_player: Query<&Player>,
    mut input: EventReader<InputEvent>,
    dt: Res<Time>,
) {
    for (mut transform, parent) in query_camera.iter_mut() {
        if let Ok(player) = query_player.get(**parent) {
            for input_event in input.read() {
                if let InputEvent::Look(look) = input_event {
                    let (mut yaw, mut pitch, _) = transform.rotation.to_euler(EulerRot::YXZ);
                    yaw -= (look.x * player.look_speed).to_radians() * dt.delta_seconds();
                    pitch -= (look.y * player.look_speed).to_radians() * dt.delta_seconds();
                    transform.rotation = -Quat::from_axis_angle(Vec3::Y, yaw)
                        * Quat::from_axis_angle(Vec3::X, pitch);
                }
            }
        }
    }
}
