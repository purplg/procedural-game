use bevy::{input::mouse::MouseMotion, prelude::*};

pub struct InputPlugin;

#[derive(Debug, Event)]
pub enum InputEvent {
    Move(Vec2),
    Jump,
    Crouch,
    Sprint(bool),
    Look(Vec2),
    Grab(bool),
    Quit,
}

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<InputEvent>()
            .add_systems(Update, mouse_move)
            .add_systems(Update, mouse_button)
            .add_systems(Update, keyboard);
    }
}

fn mouse_move(
    mut mouse_events: EventReader<MouseMotion>,
    mut input_event: EventWriter<InputEvent>,
) {
    input_event.send_batch(
        mouse_events
            .read()
            .map(|mouse_event| InputEvent::Look(mouse_event.delta)),
    );
}

fn mouse_button(mut input_event: EventWriter<InputEvent>, buttons: Res<ButtonInput<MouseButton>>) {
    for button in buttons.get_just_pressed() {
        if MouseButton::Right == *button {
            input_event.send(InputEvent::Grab(true));
        }
    }
    for button in buttons.get_just_released() {
        if MouseButton::Right == *button {
            input_event.send(InputEvent::Grab(false));
        }
    }
}

fn keyboard(keys: Res<ButtonInput<KeyCode>>, mut event_writer: EventWriter<InputEvent>) {
    if keys.pressed(KeyCode::KeyW) {
        event_writer.send(InputEvent::Move(Vec2::X));
    } else if keys.pressed(KeyCode::KeyS) {
        event_writer.send(InputEvent::Move(-Vec2::X));
    }

    if keys.pressed(KeyCode::Space) {
        event_writer.send(InputEvent::Jump);
    }

    if keys.pressed(KeyCode::KeyC) {
        event_writer.send(InputEvent::Crouch);
    }

    if keys.pressed(KeyCode::KeyD) {
        event_writer.send(InputEvent::Move(Vec2::Y));
    } else if keys.pressed(KeyCode::KeyA) {
        event_writer.send(InputEvent::Move(-Vec2::Y));
    }

    if keys.just_pressed(KeyCode::ShiftLeft) {
        event_writer.send(InputEvent::Sprint(true));
    } else if keys.just_released(KeyCode::ShiftLeft) {
        event_writer.send(InputEvent::Sprint(false));
    }

    if keys.just_pressed(KeyCode::Escape) {
        event_writer.send(InputEvent::Quit);
    }
}
