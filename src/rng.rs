use bevy::prelude::*;
use rand::{rngs::SmallRng, SeedableRng};

pub struct RngPlugin;

impl Plugin for RngPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<RngSource>();
    }
}

#[derive(Deref, DerefMut, Resource)]
pub struct RngSource {
    rng: SmallRng,
}

impl Default for RngSource {
    fn default() -> Self {
        Self {
            rng: SmallRng::from_entropy(),
        }
    }
}
