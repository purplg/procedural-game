use bevy::prelude::*;

use super::{quad::Quad, Block, FaceData};
use crate::assets::textures::DynamicTextures;

#[derive(Debug, Copy, Clone)]
pub struct Wall;

impl Block for Wall {
    fn is_transparent(&self) -> bool {
        false
    }

    fn north_face(&self, txt: &DynamicTextures, _height: u8) -> Option<FaceData> {
        Some((Quad::facing_north(&txt.get("bricks").unwrap())).into())
    }

    fn east_face(&self, txt: &DynamicTextures, _height: u8) -> Option<FaceData> {
        Some((Quad::facing_east(&txt.get("bricks").unwrap()) + Vec3::X).into())
    }

    fn south_face(&self, txt: &DynamicTextures, _height: u8) -> Option<FaceData> {
        Some((Quad::facing_south(&txt.get("bricks").unwrap()) + Vec3::Z).into())
    }

    fn west_face(&self, txt: &DynamicTextures, _height: u8) -> Option<FaceData> {
        Some((Quad::facing_west(&txt.get("bricks").unwrap())).into())
    }
}
