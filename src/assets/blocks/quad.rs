use std::ops::{Add, Mul};

use bevy::prelude::*;

use super::{FaceData, Vertex};
use crate::{assets::textures::UVRect, world::position::GridPosition};

#[derive(Clone)]
pub struct Quad {
    pub vertices: [Vertex; 4],
    pub indices: [u32; 6],
}

impl Quad {
    pub fn facing_north(uvs: &UVRect) -> Self {
        let normal = Vec3::NEG_Z.to_array();
        Self {
            vertices: [
                Vertex {
                    position: [0.0, 1.0, 0.0],
                    normal,
                    uv: uvs.top_left(),
                },
                Vertex {
                    position: [0.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_left(),
                },
                Vertex {
                    position: [1.0, 1.0, 0.0],
                    normal,
                    uv: uvs.top_right(),
                },
                Vertex {
                    position: [1.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_right(),
                },
            ],
            indices: [2, 3, 1, 2, 1, 0],
        }
    }

    pub fn facing_east(uvs: &UVRect) -> Self {
        let normal = Vec3::X.to_array();
        Self {
            vertices: [
                Vertex {
                    position: [0.0, 1.0, 1.0],
                    normal,
                    uv: uvs.top_left(),
                },
                Vertex {
                    position: [0.0, 1.0, 0.0],
                    normal,
                    uv: uvs.top_right(),
                },
                Vertex {
                    position: [0.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_right(),
                },
                Vertex {
                    position: [0.0, 0.0, 1.0],
                    normal,
                    uv: uvs.bottom_left(),
                },
            ],
            indices: [1, 3, 2, 1, 0, 3],
        }
    }

    pub fn facing_south(uvs: &UVRect) -> Self {
        let normal = Vec3::Z.to_array();
        Self {
            vertices: [
                Vertex {
                    position: [0.0, 1.0, 0.0],
                    normal,
                    uv: uvs.top_left(),
                },
                Vertex {
                    position: [0.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_left(),
                },
                Vertex {
                    position: [1.0, 1.0, 0.0],
                    normal,
                    uv: uvs.top_right(),
                },
                Vertex {
                    position: [1.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_right(),
                },
            ],
            indices: [0, 1, 2, 1, 3, 2],
        }
    }

    pub fn facing_west(uvs: &UVRect) -> Self {
        let normal = Vec3::NEG_X.to_array();
        Self {
            vertices: [
                Vertex {
                    position: [0.0, 1.0, 1.0],
                    normal,
                    uv: uvs.top_right(),
                },
                Vertex {
                    position: [0.0, 1.0, 0.0],
                    normal,
                    uv: uvs.top_left(),
                },
                Vertex {
                    position: [0.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_left(),
                },
                Vertex {
                    position: [0.0, 0.0, 1.0],
                    normal,
                    uv: uvs.bottom_right(),
                },
            ],
            indices: [3, 0, 1, 2, 3, 1],
        }
    }

    pub fn facing_up(uvs: &UVRect) -> Self {
        let normal = Vec3::Y.to_array();
        Self {
            vertices: [
                Vertex {
                    position: [0.0, 0.0, 0.0],
                    normal,
                    uv: uvs.top_left(),
                },
                Vertex {
                    position: [0.0, 0.0, 1.0],
                    normal,
                    uv: uvs.top_right(),
                },
                Vertex {
                    position: [1.0, 0.0, 1.0],
                    normal,
                    uv: uvs.bottom_right(),
                },
                Vertex {
                    position: [1.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_left(),
                },
            ],
            indices: [2, 3, 1, 1, 3, 0],
        }
    }

    pub fn facing_down(uvs: &UVRect) -> Self {
        let normal = Vec3::NEG_Y.to_array();
        Self {
            vertices: [
                Vertex {
                    position: [0.0, 0.0, 0.0],
                    normal,
                    uv: uvs.top_left(),
                },
                Vertex {
                    position: [0.0, 0.0, 1.0],
                    normal,
                    uv: uvs.top_right(),
                },
                Vertex {
                    position: [1.0, 0.0, 1.0],
                    normal,
                    uv: uvs.bottom_right(),
                },
                Vertex {
                    position: [1.0, 0.0, 0.0],
                    normal,
                    uv: uvs.bottom_left(),
                },
            ],
            indices: [0, 3, 1, 1, 3, 2],
        }
    }

    pub fn down(uvs: &UVRect) -> Self {
        let normal = Vec3::NEG_Y.to_array();
        Self {
            vertices: [
                Vertex {
                    position: [0.0, 1.0, 0.0],
                    normal,
                    uv: uvs.top_left(),
                },
                Vertex {
                    position: [0.0, 1.0, 1.0],
                    normal,
                    uv: uvs.top_right(),
                },
                Vertex {
                    position: [1.0, 1.0, 1.0],
                    normal,
                    uv: uvs.bottom_right(),
                },
                Vertex {
                    position: [1.0, 1.0, 0.0],
                    normal,
                    uv: uvs.bottom_left(),
                },
            ],
            indices: [0, 3, 1, 1, 3, 2],
        }
    }
}

impl From<Quad> for FaceData {
    fn from(quad: Quad) -> Self {
        Self {
            vertices: quad.vertices.to_vec(),
            indices: quad.indices.to_vec(),
        }
    }
}

impl<P: GridPosition> Add<P> for Quad {
    type Output = Quad;

    fn add(mut self, pos: P) -> Self::Output {
        self.vertices.iter_mut().for_each(|v| {
            v.position[0] += pos.x() as f32;
            v.position[1] += pos.y() as f32;
            v.position[2] += pos.z() as f32;
        });
        self
    }
}

impl Add<Quad> for Quad {
    type Output = FaceData;

    fn add(self, other: Quad) -> Self::Output {
        Into::<FaceData>::into(self) + Into::<FaceData>::into(other)
    }
}

impl Add<Vec3> for Quad {
    type Output = Quad;

    fn add(mut self, pos: Vec3) -> Self::Output {
        self.vertices.iter_mut().for_each(|v| {
            v.position[0] += pos.x;
            v.position[1] += pos.y;
            v.position[2] += pos.z;
        });
        self
    }
}

impl Mul<Vec3> for Quad {
    type Output = Quad;

    fn mul(mut self, pos: Vec3) -> Self::Output {
        self.vertices.iter_mut().for_each(|v| {
            v.position[0] *= pos.x;
            v.position[1] *= pos.y;
            v.position[2] *= pos.z;
        });
        self
    }
}

impl Mul<f32> for Quad {
    type Output = Quad;

    fn mul(self, pos: f32) -> Self::Output {
        self * Vec3::splat(pos)
    }
}
