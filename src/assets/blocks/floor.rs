use bevy::prelude::Vec3;

use super::{quad::Quad, Block, FaceData};
use crate::assets::textures::DynamicTextures;

#[derive(Debug, Copy, Clone)]
pub struct Floor;

impl Block for Floor {
    fn is_transparent(&self) -> bool {
        true
    }

    fn top_face(&self, txt: &DynamicTextures, height: u8) -> Option<FaceData> {
        Some((Quad::facing_down(&txt.get("dirt").unwrap()) + Vec3::Y * height as f32).into())
    }

    fn bottom_face(&self, txt: &DynamicTextures, _height: u8) -> Option<FaceData> {
        Some((Quad::facing_up(&txt.get("dirt").unwrap())).into())
    }
}
