use bevy::prelude::Vec3;

use super::{quad::Quad, Block, FaceData};
use crate::assets::textures::DynamicTextures;

#[derive(Debug, Copy, Clone)]
pub struct Hole;

impl Block for Hole {
    fn is_transparent(&self) -> bool {
        true
    }
}
