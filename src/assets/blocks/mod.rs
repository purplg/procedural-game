mod directional;
mod door;
mod floor;
mod hole;
pub mod quad;
mod wall;

use std::ops::Add;

use bevy::prelude::*;
use hole::Hole;

use self::{directional::Directional, door::Door, floor::Floor, wall::Wall};
use super::textures::DynamicTextures;
use crate::world::position::GridPosition;

pub enum Facing {
    North,
    East,
    South,
    West,
    Up,
    Down,
}

#[derive(Debug, Copy, Clone)]
pub enum BlockId {
    Door,
    Floor,
    Wall,
    Directional,
    Hole,
}

impl BlockId {
    pub fn is_transparent(self) -> bool {
        match self {
            BlockId::Door => Door.is_transparent(),
            BlockId::Wall => Wall.is_transparent(),
            BlockId::Directional => Directional.is_transparent(),
            BlockId::Floor => Floor.is_transparent(),
            BlockId::Hole => Hole.is_transparent(),
        }
    }

    pub fn block(self) -> Box<dyn Block> {
        match self {
            BlockId::Door => Box::new(Door),
            BlockId::Wall => Box::new(Wall),
            BlockId::Directional => Box::new(Directional),
            BlockId::Floor => Box::new(Floor),
            BlockId::Hole => Box::new(Hole),
        }
    }

    pub fn face(self, face: Facing, mat: &DynamicTextures, height: u8) -> Option<FaceData> {
        match face {
            Facing::North => self.block().north_face(mat, height),
            Facing::East => self.block().east_face(mat, height),
            Facing::South => self.block().south_face(mat, height),
            Facing::West => self.block().west_face(mat, height),
            Facing::Up => self.block().top_face(mat, height),
            Facing::Down => self.block().bottom_face(mat, height),
        }
    }
}

#[derive(Clone)]
pub struct Vertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
    pub uv: [f32; 2],
}

#[derive(Clone)]
pub struct FaceData {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
}

impl Add<FaceData> for FaceData {
    type Output = FaceData;

    fn add(mut self, mut other: FaceData) -> Self::Output {
        let offset = self.vertices.len() as u32;
        self.vertices.append(&mut other.vertices);
        self.indices
            .append(&mut other.indices.into_iter().map(|i| i + offset).collect());
        self
    }
}

impl<P: GridPosition> Add<P> for FaceData {
    type Output = FaceData;

    fn add(mut self, pos: P) -> Self::Output {
        self.vertices.iter_mut().for_each(|v| {
            v.position[0] += pos.as_vec3().x;
            v.position[1] += pos.as_vec3().y;
            v.position[2] += pos.as_vec3().z;
        });
        self
    }
}

impl Add<Vec3> for FaceData {
    type Output = FaceData;

    fn add(mut self, pos: Vec3) -> Self::Output {
        self.vertices.iter_mut().for_each(|v| {
            v.position[0] += pos.x;
            v.position[1] += pos.y;
            v.position[2] += pos.z;
        });
        self
    }
}

pub trait Block {
    fn is_transparent(&self) -> bool;

    fn north_face(&self, _mat: &DynamicTextures, _height: u8) -> Option<FaceData> {
        None
    }

    fn east_face(&self, _mat: &DynamicTextures, _height: u8) -> Option<FaceData> {
        None
    }

    fn south_face(&self, _mat: &DynamicTextures, _height: u8) -> Option<FaceData> {
        None
    }

    fn west_face(&self, _mat: &DynamicTextures, _height: u8) -> Option<FaceData> {
        None
    }

    fn top_face(&self, _mat: &DynamicTextures, _height: u8) -> Option<FaceData> {
        None
    }

    fn bottom_face(&self, _mat: &DynamicTextures, _height: u8) -> Option<FaceData> {
        None
    }
}
