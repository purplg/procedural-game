use bevy::prelude::*;

use super::{quad::Quad, Block, FaceData};
use crate::assets::textures::DynamicTextures;

#[derive(Debug, Copy, Clone)]
pub struct Door;

impl Block for Door {
    fn is_transparent(&self) -> bool {
        true
    }

    // fn west_face(&self, position: &LocalPosition, txt: &WorldMaterial) -> Option<FaceData> {
    //     Some((Quad::facing_west(&txt.get("door").unwrap()) + position + Vec3::X * 0.4).into())
    // }

    // fn east_face(&self, position: &LocalPosition, txt: &WorldMaterial) -> Option<FaceData> {
    //     Some((Quad::facing_east(&txt.get("door").unwrap()) + position + Vec3::X * 0.6).into())
    // }

    // fn top_face(&self, position: &LocalPosition, txt: &WorldMaterial) -> Option<FaceData> {
    //     Some(
    //         (Quad::facing_up(&txt.get("door").unwrap()) * Vec3::new(0.2, 1.0, 1.0)
    //             + position
    //             + Vec3::Y
    //             + Vec3::X * 0.4)
    //             .into(),
    //     )
    // }

    fn top_face(&self, txt: &DynamicTextures, height: u8) -> Option<FaceData> {
        Some((Quad::facing_down(&txt.get("dirt").unwrap()) + Vec3::Y * height as f32).into())
    }

    fn bottom_face(&self, txt: &DynamicTextures, _height: u8) -> Option<FaceData> {
        Some((Quad::facing_up(&txt.get("dirt").unwrap())).into())
    }
}
