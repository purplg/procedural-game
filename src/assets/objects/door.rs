use bevy::prelude::*;

use super::{IntoMesh, IntoMeshData};
use crate::{
    assets::{blocks::quad::Quad, textures::DynamicTextures},
    world::chunk::mesh::MeshData,
};

#[derive(Component)]
pub struct Door(pub(super) u8);

impl IntoMeshData for Door {
    fn mesh_data(txt: &DynamicTextures) -> MeshData {
        let down = Quad::facing_down(&txt.get("door").unwrap());
        let west = Quad::facing_west(&txt.get("door").unwrap()) + Vec3::X * 0.4;
        let east = Quad::facing_east(&txt.get("door").unwrap()) + Vec3::X * 0.6;
        let up = Quad::facing_up(&txt.get("door").unwrap()) * Vec3::new(0.2, 1.0, 1.0) + Vec3::Y + Vec3::X * 0.4;

        let mut mesh_data = MeshData::default();
        mesh_data.push(Vec3::ONE, down);
        mesh_data.push(Vec3::ONE, west);
        mesh_data.push(Vec3::ONE, east);
        mesh_data.push(Vec3::ONE, up);
        mesh_data
    }
}

#[derive(Component)]
pub struct Key(pub(super) u8);

impl IntoMesh for Key {
    fn mesh(_txt: &DynamicTextures, scale: f32) -> Mesh {
        Torus {
            major_radius: 0.5 * scale,
            minor_radius: 0.1 * scale,
        }
        .into()
    }
}
