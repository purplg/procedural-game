use bevy::{
    ecs::system::EntityCommands,
    prelude::*,
    render::{mesh::Indices, render_asset::RenderAssetUsages, render_resource::PrimitiveTopology},
};

use super::textures::DynamicTextures;
use crate::world::chunk::mesh::MeshData;

mod door;

pub use door::{Door, Key};

#[derive(Clone, Copy)]
pub enum ObjectId {
    Door(u8),
    Key(u8),
}

impl ObjectId {
    pub fn insert(&self, commands: &mut EntityCommands) {
        match self {
            ObjectId::Door(id) => {
                commands.insert(Door(*id));
            }
            ObjectId::Key(id) => {
                commands.insert(Key(*id));
            }
        }
    }

    pub fn mesh(&self, txt: &DynamicTextures, scale: f32) -> Mesh {
        match self {
            ObjectId::Door(_id) => Door::mesh(txt, scale),
            ObjectId::Key(_id) => Key::mesh(txt, scale),
        }
    }
}

pub trait IntoMeshData {
    fn mesh_data(_txt: &DynamicTextures) -> MeshData;
}

pub trait IntoMesh {
    fn mesh(_txt: &DynamicTextures, scale: f32) -> Mesh;
}

impl<T> IntoMesh for T
where
    T: IntoMeshData,
{
    fn mesh(txt: &DynamicTextures, scale: f32) -> Mesh {
        let mesh_data = Self::mesh_data(txt) * scale;
        let mut mesh = Mesh::new(PrimitiveTopology::TriangleList, RenderAssetUsages::all());
        mesh.insert_indices(Indices::U32(mesh_data.indices.clone()));
        mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, mesh_data.positions);
        mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, mesh_data.normals);
        mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, mesh_data.uvs);
        mesh
    }
}
