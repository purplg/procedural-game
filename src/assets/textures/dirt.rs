use image::Rgb;
use imageproc::drawing::Canvas;
use rand::RngCore;

use super::DynTexture;

pub(crate) struct Dirt;

impl Dirt {
    pub fn new() -> Self {
        Self
    }
}

impl<R, C> DynTexture<R, C> for Dirt
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "dirt"
    }

    fn draw(&self, rng: &mut R, canvas: &mut C) {
        for x in 0..canvas.width() {
            for y in 0..canvas.height() {
                let v = (rng.next_u32() % 100) as f32 / 100.0;
                canvas.draw_pixel(
                    x,
                    y,
                    Rgb([(v * 155.0) as u8, (v * 118.0) as u8, (v * 83.0) as u8]),
                );
            }
        }
    }
}
