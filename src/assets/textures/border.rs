use image::Rgb;
use imageproc::drawing::Canvas;
use rand::RngCore;

use super::DynTexture;

pub(crate) struct Border;

impl<R, C> DynTexture<R, C> for Border
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "border"
    }

    fn draw(&self, _rng: &mut R, canvas: &mut C) {
        let base = [0, 0, 255];
        let border = [0, 255, 0];
        for x in 0..canvas.width() {
            for y in 0..canvas.height() {
                canvas.draw_pixel(
                    x,
                    y,
                    Rgb(
                        if x == 0 || x == canvas.width() - 1 || y == 0 || y == canvas.height() - 1 {
                            border
                        } else {
                            base
                        },
                    ),
                );
            }
        }
    }
}
