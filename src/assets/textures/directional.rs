use image::Rgb;
use imageproc::drawing::Canvas;
use rand::RngCore;

use super::DynTexture;

pub(crate) struct North;

impl<R, C> DynTexture<R, C> for North
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "north"
    }

    fn draw(&self, _rng: &mut R, canvas: &mut C) {
        for x in 0..canvas.width() {
            for y in 0..canvas.height() {
                canvas.draw_pixel(x, y, Rgb([0, 0, 255]));
            }
        }
    }
}

pub(crate) struct East;

impl<R, C> DynTexture<R, C> for East
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "east"
    }

    fn draw(&self, _rng: &mut R, canvas: &mut C) {
        for x in 0..canvas.width() {
            for y in 0..canvas.height() {
                canvas.draw_pixel(x, y, Rgb([255, 0, 0]));
            }
        }
    }
}

pub(crate) struct South;

impl<R, C> DynTexture<R, C> for South
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "south"
    }

    fn draw(&self, _rng: &mut R, canvas: &mut C) {
        for x in 0..canvas.width() {
            for y in 0..canvas.height() {
                canvas.draw_pixel(x, y, Rgb([0, 0, 100]));
            }
        }
    }
}

pub(crate) struct West;

impl<R, C> DynTexture<R, C> for West
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "west"
    }

    fn draw(&self, _rng: &mut R, canvas: &mut C) {
        for x in 0..canvas.width() {
            for y in 0..canvas.height() {
                canvas.draw_pixel(x, y, Rgb([100, 0, 0]));
            }
        }
    }
}
