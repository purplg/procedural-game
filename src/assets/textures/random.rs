use image::Rgb;
use imageproc::drawing::Canvas;
use rand::RngCore;

use super::DynTexture;

pub(crate) struct Random;

impl<R, C> DynTexture<R, C> for Random
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "random"
    }

    fn draw(&self, rng: &mut R, canvas: &mut C) {
        for x in 0..canvas.width() {
            for y in 0..canvas.height() {
                canvas.draw_pixel(
                    x,
                    y,
                    Rgb([
                        (rng.next_u32() % 255) as u8,
                        (rng.next_u32() % 255) as u8,
                        (rng.next_u32() % 255) as u8,
                    ]),
                );
            }
        }
    }
}
