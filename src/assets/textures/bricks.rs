use image::Rgb;
use imageproc::{
    drawing::{draw_filled_rect_mut, draw_line_segment_mut, Canvas},
    rect::Rect,
};
use rand::RngCore;

use super::DynTexture;

pub(crate) struct Bricks;

impl Bricks {
    pub fn new() -> Self {
        Self
    }
}

impl<R, C> DynTexture<R, C> for Bricks
where
    R: RngCore,
    C: Canvas<Pixel = Rgb<u8>>,
{
    fn name(&self) -> &'static str {
        "bricks"
    }

    fn draw(&self, rng: &mut R, canvas: &mut C) {
        let rng1 = rng.next_u32();
        let rng2 = rng.next_u32();

        let mut power = 0u32;
        let mut w = canvas.width();
        while w & 1 != 1 {
            power += 1;
            w >>= 1;
        }

        let h_bricks = canvas.width() / 2_u32.pow((rng1 % (power - 1)) + 1);
        let brick_width = (canvas.width() / h_bricks) as f32;
        let layers = (rng2 % 2 + 1) * 2;
        let brick_height = (canvas.height() as f32 / layers as f32).round();

        let fill = Rgb([
            (rng.next_u32() % 255) as u8,
            (rng.next_u32() % 255) as u8,
            (rng.next_u32() % 255) as u8,
        ]);
        let outline = Rgb([
            fill.0[0].saturating_sub(50),
            fill.0[1].saturating_sub(50),
            fill.0[2].saturating_sub(50),
        ]);

        draw_filled_rect_mut(
            canvas,
            Rect::at(0, 0).of_size(canvas.width(), canvas.height()),
            fill,
        );

        for layer in 0..layers {
            let y = brick_height * layer as f32;
            draw_line_segment_mut(canvas, (0.0, y), (canvas.width() as f32, y), outline);

            let alt = layer % 2;
            for x in 0..h_bricks + (1 - alt) {
                let x = alt as f32 * brick_width * 0.5 + brick_width * x as f32;
                draw_line_segment_mut(canvas, (x, y), (x, y + brick_height), outline);
            }
        }
    }
}
