mod border;
mod bricks;
mod directional;
mod dirt;
mod door;
mod random;

use std::ops::Deref;

use bevy::{prelude::*, render::render_asset::RenderAssetUsages, utils::HashMap};
pub(crate) use border::Border;
pub(crate) use bricks::Bricks;
pub(crate) use directional::{East, North, South, West};
pub(crate) use dirt::Dirt;
pub(crate) use door::Door;
use image::{Rgb, RgbImage};
use imageproc::drawing::Canvas;
use rand::{rngs::SmallRng, RngCore};

use crate::rng::RngSource;

pub const TEXTURE_SIZE: u32 = 16;

pub trait DynTexture<R: RngCore, C: Canvas<Pixel = Rgb<u8>>> {
    fn name(&self) -> &'static str;

    fn draw(&self, rng: &mut R, canvas: &mut C);
}

#[derive(Debug)]
pub struct UVRect(Rect);

impl UVRect {
    pub fn new(min: Vec2, max: Vec2) -> Self {
        Self(Rect::from_corners(min, max))
    }

    pub fn from_atlas(
        atlas: &TextureAtlas,
        image: Handle<Image>,
        texture_atlases: &Assets<TextureAtlasLayout>,
    ) -> Self {
        let atlas = texture_atlases.get(atlas.layout.id()).unwrap();
        let i = atlas.get_texture_index(&image).unwrap() as u32;
        let tile_w = atlas.size.x as u32 / TEXTURE_SIZE;
        let tile_h = atlas.size.y as u32 / TEXTURE_SIZE;
        let tile_x = i % tile_w;
        let tile_y = i / tile_w;

        let offset = (1.0 / TEXTURE_SIZE as f32) / atlas.size.x as f32;
        let x1 = tile_x as f32 / tile_w as f32;
        let y1 = tile_y as f32 / tile_h as f32;
        let x2 = x1 + (TEXTURE_SIZE as f32 / atlas.size.x as f32);
        let y2 = y1 + (TEXTURE_SIZE as f32 / atlas.size.y as f32);

        Self::new(
            Vec2::new(x1 + offset, y1 + offset),
            Vec2::new(x2 - offset, y2 - offset),
        )
    }

    pub const fn top_left(&self) -> [f32; 2] {
        [self.0.min.x, self.0.min.y]
    }

    pub const fn top_right(&self) -> [f32; 2] {
        [self.0.max.x, self.0.min.y]
    }

    pub const fn bottom_right(&self) -> [f32; 2] {
        [self.0.max.x, self.0.max.y]
    }

    pub const fn bottom_left(&self) -> [f32; 2] {
        [self.0.min.x, self.0.max.y]
    }
}

#[derive(Deref)]
pub struct Textures(pub HashMap<&'static str, UVRect>);

pub struct DynamicTexturesPlugin;

impl Plugin for DynamicTexturesPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(PreStartup, setup);
    }
}

#[derive(Resource)]
pub struct DynamicTextures {
    pub material: Handle<StandardMaterial>,
    pub textures: Textures,
}

impl Deref for DynamicTextures {
    type Target = Textures;

    fn deref(&self) -> &Self::Target {
        &self.textures
    }
}

fn setup(
    mut commands: Commands,
    mut images: ResMut<Assets<Image>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut rng: ResMut<RngSource>,
    mut atlases: ResMut<Assets<TextureAtlasLayout>>,
) {
    let rng = &mut **rng;
    let images = &mut images;

    let mut atlas_builder = TextureAtlasBuilder::default();

    let dyntextures: Vec<Box<dyn DynTexture<SmallRng, RgbImage>>> = vec![
        Box::new(Dirt),
        Box::new(Bricks),
        Box::new(North),
        Box::new(East),
        Box::new(South),
        Box::new(West),
        Box::new(Door),
        Box::new(Border),
    ];

    let mut texture_images: HashMap<&'static str, Image> = HashMap::default();
    for dyntexture in dyntextures {
        debug!("Generating {}", dyntexture.name());
        let mut canvas =
            RgbImage::from_pixel(TEXTURE_SIZE as u32, TEXTURE_SIZE as u32, Rgb([255, 0, 255]));
        dyntexture.draw(rng, &mut canvas);
        let image = Image::from_dynamic(
            image::DynamicImage::ImageRgb8(canvas),
            true,
            RenderAssetUsages::all(),
        );
        texture_images.insert(dyntexture.name(), image);
    }

    let mut texture_handles: HashMap<&'static str, Handle<Image>> = HashMap::default();
    for (name, image) in &texture_images {
        debug!("Creating handle for {}", name);
        let handle = images.add(image.clone());
        atlas_builder.add_texture(Some(handle.id()), &image);
        texture_handles.insert(name, handle);
    }

    let (atlas_layout, image): (TextureAtlasLayout, Image) = atlas_builder.build().unwrap();
    let handle_atlas: Handle<TextureAtlasLayout> = atlases.add(atlas_layout);
    let atlas: TextureAtlas = handle_atlas.into();

    let mut textures: HashMap<&'static str, UVRect> = HashMap::default();
    for (name, handle) in &texture_handles {
        debug!("Getting UVRect for {}", name);
        textures.insert(name, UVRect::from_atlas(&atlas, handle.clone(), &atlases));
    }
    let textures = Textures(textures);
    let image = images.add(image);

    commands.insert_resource(DynamicTextures {
        material: materials.add(StandardMaterial {
            base_color_texture: Some(image),
            perceptual_roughness: 1.0,
            ..default()
        }),
        textures,
    });

    info!("Textures generated");
}

pub trait IntoImage {
    fn into_image(self) -> Image
    where
        Self: Sized;
}
