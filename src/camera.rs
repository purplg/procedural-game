use bevy::{
    prelude::*,
    window::{CursorGrabMode, PrimaryWindow},
};
use bevy_inspector_egui::prelude::*;

use crate::{gamestate::GameState, input::InputEvent};

pub struct CameraPlugin;

#[derive(Component, Reflect, InspectorOptions)]
#[reflect(InspectorOptions)]
struct FlyCamera {
    #[inspector(min = 0.0)]
    move_speed: f32,
    #[inspector(min = 0.0)]
    look_speed: f32,
}

impl FlyCamera {
    fn new(move_speed: f32, look_speed: f32) -> Self {
        Self {
            move_speed,
            look_speed,
        }
    }
}

#[derive(Component, Reflect, InspectorOptions)]
#[reflect(InspectorOptions)]
struct Sprint {
    sprinting: bool,
    multiplier: f32,
}

impl Sprint {
    pub fn new(multiplier: f32) -> Self {
        Self {
            sprinting: false,
            multiplier,
        }
    }

    fn speed(&self, base_speed: f32) -> f32 {
        if self.sprinting {
            base_speed * self.multiplier
        } else {
            base_speed
        }
    }
}

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<FlyCamera>()
            .insert_resource(ClearColor(Color::srgb(0.01, 0.01, 0.01)))
            .add_systems(Startup, setup)
            .add_systems(
                Update,
                (input_movement, input_look)
                    .chain()
                    .run_if(|gamestate: Res<State<GameState>>| gamestate.is_controlling()),
            )
            .add_systems(Update, cursor_grab)
            .add_systems(PreUpdate, input_sprint);
    }
}

fn setup(mut commands: Commands) {
    let start_pos = Transform {
        translation: Vec3::new(167.143, 431.618, 188.064),
        rotation: Quat::from_euler(EulerRot::XYZ, -1.502, 0.0, 0.0),
        scale: Vec3::ONE,
    };
    commands.spawn((
        Camera3dBundle {
            transform: start_pos,
            ..default()
        },
        FlyCamera::new(20.0, 5.0),
        Sprint::new(5.0),
    ));
}

fn cursor_grab(
    mut window: Query<&mut Window, With<PrimaryWindow>>,
    mut input: EventReader<InputEvent>,
) {
    if let Ok(mut window) = window.get_single_mut() {
        for input in input.read() {
            if let InputEvent::Grab(grab) = input {
                if *grab {
                    window.cursor.grab_mode = CursorGrabMode::Confined;
                    window.cursor.visible = false;
                } else {
                    window.cursor.grab_mode = CursorGrabMode::None;
                    window.cursor.visible = true;
                };
            }
        }
    }
}

fn input_sprint(mut query: Query<&mut Sprint>, mut input: EventReader<InputEvent>, _dt: Res<Time>) {
    for mut sprint in query.iter_mut() {
        for input_event in input.read() {
            if let InputEvent::Sprint(value) = input_event {
                sprint.sprinting = *value;
            }
        }
    }
}

fn input_movement(
    mut query: Query<(&mut Transform, &FlyCamera, Option<&Sprint>)>,
    mut input: EventReader<InputEvent>,
    dt: Res<Time>,
) {
    for (mut transform, cam, sprint) in query.iter_mut() {
        let direction: Vec3 = input
            .read()
            .map(|event| match event {
                InputEvent::Move(movement) => {
                    (transform.forward() * movement.x) + (transform.right() * movement.y)
                }
                InputEvent::Jump => Vec3::Y,
                InputEvent::Crouch => Vec3::NEG_Y,
                _ => Vec3::ZERO,
            })
            .sum();

        transform.translation += direction
            * sprint.map_or(cam.move_speed, |sprint| sprint.speed(cam.move_speed))
            * dt.delta_seconds();
    }
}

fn input_look(
    mut query: Query<(&mut Transform, &FlyCamera)>,
    mut input: EventReader<InputEvent>,
    dt: Res<Time>,
) {
    for (mut target, cam) in query.iter_mut() {
        let look: Vec2 = input
            .read()
            .map(|event| match event {
                InputEvent::Look(look) => *look,
                _ => Vec2::ZERO,
            })
            .sum();

        let (mut yaw, mut pitch, _) = target.rotation.to_euler(EulerRot::YXZ);
        yaw -= (look.x * cam.look_speed).to_radians() * dt.delta_seconds();
        pitch -= (look.y * cam.look_speed).to_radians() * dt.delta_seconds();
        target.rotation =
            Quat::from_axis_angle(Vec3::Y, yaw) * Quat::from_axis_angle(Vec3::X, pitch);
    }
}
