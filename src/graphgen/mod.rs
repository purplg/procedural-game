#![allow(dead_code)]

pub mod graph;
pub mod mission;
pub mod shape;

#[cfg(test)]
mod tests {
    use std::{fs::File, io::Write};

    use mission::{linear, start};
    use petgraph::dot::Dot;
    use rand::{rngs::SmallRng, RngCore, SeedableRng};

    use super::*;
    use crate::graphgen::mission::{finish, hook, new_mission, parallel};

    #[test]
    fn it_works() {
        let mut rng = SmallRng::from_entropy();
        let seed = rng.next_u64();
        println!("seed: {:?}", seed);
        let graph = new_mission(
            &mut SmallRng::seed_from_u64(seed),
            vec![
                start::all(),
                finish::all(),
                linear::all(),
                hook::all(),
                parallel::all(),
            ],
        );

        println!(
            "NonTerminal remaining: {:?}",
            graph.node_weights().filter(|node| !node.terminal()).count()
        );

        let mut file = File::create("/tmp/procedural-nodes.dot").unwrap();
        file.write_all(&format!("{:?}", Dot::with_config(&graph, &[])).into_bytes())
            .unwrap();
    }
}
