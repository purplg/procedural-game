use std::collections::HashSet;
use std::marker::PhantomData;

use petgraph::data::DataMap;
use petgraph::graph::DiGraph;
use petgraph::visit::{Bfs, IntoNeighbors, Visitable};
use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use rand::{Rng, RngCore, SeedableRng};

use crate::direction::Direction;
use crate::graphgen::graph::MissionNode;
use crate::graphgen::mission::MissionGraph;

use ndarray::Array2;

type Grid = Array2<Option<Room>>;

#[derive(Default)]
pub struct ShapeBuilder {
    grid: Grid,

    origin: (usize, usize),

    /// The history of room placement
    ///
    /// This is used to traverse "backwards" when the latest placed
    /// room runs out of neighbors.
    stack: Vec<(usize, usize)>,
}

impl ShapeBuilder {
    pub fn new(entrance: Room) -> Self {
        let mut builder = Self {
            grid: Grid::default((10, 10)),
            origin: (5, 5),
            stack: Default::default(),
        };
        builder.set_room(builder.origin, entrance);
        builder
    }

    fn set_room(&mut self, pos: (usize, usize), room: Room) {
        self.grid.get_mut(pos).unwrap().replace(room);
        self.stack.push(pos);
    }

    fn get_room_mut(&mut self, pos: (usize, usize)) -> Option<&mut Room> {
        self.grid.get_mut(pos).map(|inner| inner.as_mut()).flatten()
    }

    fn find_placement<R: Rng>(&mut self, rng: &mut R) -> (Direction, (usize, usize)) {
        while self.stack.len() > 0 {
            let (x, y) = self.stack.remove(rng.gen_range(0..self.stack.len()));

            let mut possible = vec![];

            if x > 0 {
                if self
                    .grid
                    .get((x as usize - 1, y as usize))
                    .unwrap()
                    .is_none()
                {
                    possible.push((Direction::West, (x - 1, y)));
                }
            }

            if y > 0 {
                if self.grid.get((x, y - 1)).unwrap().is_none() {
                    possible.push((Direction::North, (x, y - 1)));
                }
            }

            if let Some(inner) = self.grid.get((x + 1, y)) {
                if inner.is_none() {
                    possible.push((Direction::East, (x + 1, y)));
                }
            }

            if let Some(inner) = self.grid.get((x, y + 1)) {
                if inner.is_none() {
                    possible.push((Direction::South, (x, y + 1)));
                }
            }

            if possible.len() == 0 {
                self.stack.pop();
                continue;
            }

            self.stack.push((x, y));
            return *possible.choose(rng).unwrap();
        }

        panic!("Could not find room");
    }

    fn add_room<R: RngCore>(&mut self, rng: &mut R, mut room: Room) {
        let (direction, pos) = self.find_placement(rng);

        let (x, y) = pos;
        room.connections.insert(direction.opposite());
        if y > 0 {
            if let Some(neighbor) = self.get_room_mut((x, y - 1)) {
                neighbor.connections.insert(direction);
            }
        }
        match direction {
            Direction::South => {
                room.connections.insert(Direction::North);
                if y > 0 {
                    if let Some(neighbor) = self.get_room_mut((x, y - 1)) {
                        neighbor.connections.insert(Direction::South);
                    }
                }
            }
            Direction::East => {
                room.connections.insert(Direction::West);
                if x > 0 {
                    if let Some(neighbor) = self.get_room_mut((x - 1, y)) {
                        neighbor.connections.insert(Direction::East);
                    }
                }
            }
            Direction::North => {
                room.connections.insert(Direction::South);
                if let Some(neighbor) = self.get_room_mut((x, y + 1)) {
                    neighbor.connections.insert(Direction::North);
                }
            }
            Direction::West => {
                room.connections.insert(Direction::East);
                if let Some(neighbor) = self.get_room_mut((x + 1, y)) {
                    neighbor.connections.insert(Direction::West);
                }
            }
        }
        self.set_room(pos, room);
    }

    fn build(self) -> Grid {
        self.grid
    }
}

#[derive(Debug)]
pub struct Room {
    pub mission_node: MissionNode,
    pub connections: HashSet<Direction>,
}

#[derive(Debug)]
pub enum RoomKind {
    Entrance,
    Test,
    Unknown,
}

pub(crate) struct LayoutGenerator<G>
where
    G: Visitable,
{
    rng: SmallRng,
    search: Bfs<G::NodeId, G::Map>,
    builder: ShapeBuilder,
}

impl LayoutGenerator<MissionGraph> {
    pub fn new<R: RngCore>(rng: &mut R, graph: &MissionGraph) -> Self {
        let mut search = Bfs::new(graph, Default::default());
        Self {
            rng: SmallRng::seed_from_u64(rng.next_u64()),
            builder: ShapeBuilder::new(
                search
                    .next(&graph)
                    .map(|idx| {
                        let mission_node = *graph.node_weight(idx).unwrap();
                        assert!(mission_node == MissionNode::Entrance);
                        Room {
                            mission_node,
                            connections: Default::default(),
                        }
                    })
                    .unwrap(),
            ),
            search,
        }
    }

    pub fn grid(&self) -> &Grid {
        &self.builder.grid
    }

    pub fn tick(&mut self, graph: &MissionGraph) -> bool {
        if let Some(mission_index) = self.search.next(&graph) {
            let mission_node = graph.node_weight(mission_index).unwrap();

            self.builder.add_room(
                &mut self.rng,
                Room {
                    mission_node: *mission_node,
                    connections: Default::default(),
                },
            );
            return true;
        }
        false
    }
}

pub fn new_layout<R: RngCore>(rng: &mut R, graph: &MissionGraph) -> Grid {
    let mut generator = LayoutGenerator::new(rng, graph);

    while generator.tick(graph) {}

    generator.builder.build()
}
