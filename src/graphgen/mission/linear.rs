use petgraph::prelude::*;

use super::{find_edge, find_node, set_node, MissionGraph, Rule, RuleSet};
use crate::graphgen::{graph::Edge, mission::MissionNode::*};

pub fn all() -> RuleSet {
    let mut set = RuleSet::new("Linear");
    set.push_edge_rule(RepeatRule(1));
    set.push_edge_rule(RepeatRule(2));
    set.push_edge_rule(RepeatRule(3));
    set.push_node_rule(IntoTestRule);
    set.push_node_rule(IntoIBRule);
    set.push_node_rule(IntoTestSecretRule);
    set.push_edge_rule(IntoKeyLockRule);
    set.push_edge_rule(IntoKeyCLLockRule);
    set
}

/// Insert number of Linear Chains between 2 Linear Chains at match.
///
/// The inner `u8` is in addition to the Source and Target match. In other words:
/// `RepeatRule(1)` applied to `Chain -> Gate` becomes ` ->  ->
/// `
#[derive(Debug)]
pub struct RepeatRule(pub u8);

impl Rule for RepeatRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, Chain, Gate)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);
        set_node(graph, source, ChainLinear);
        set_node(graph, target, ChainLinear);

        let mut prev = source;
        for _ in 0..self.0 {
            let node = graph.add_node(ChainLinear);
            graph.add_edge(prev, node, Edge::Tight);
            prev = node;
        }

        graph.add_edge(prev, target, Edge::Tight);
    }
}

#[derive(Debug)]
pub struct IntoTestRule;

impl Rule for IntoTestRule {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_node(graph, ChainLinear)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: NodeIndex) {
        set_node(graph, node_index, Test);
    }
}

#[derive(Debug)]
pub struct IntoIBRule;

impl Rule for IntoIBRule {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_node(graph, ChainLinear)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: NodeIndex) {
        set_node(graph, node_index, Test);
        let node2 = graph.add_node(Test);
        let node3 = graph.add_node(ItemBonus);

        graph.add_edge(node2, node3, Edge::Tight);

        for (node_index, edge_index) in graph
            .edges_directed(node_index, Outgoing)
            .map(|edge_ref| (edge_ref.target(), edge_ref.id()))
            .collect::<Vec<(NodeIndex, EdgeIndex)>>()
        {
            graph.remove_edge(edge_index);
            graph.add_edge(node3, node_index, Edge::Tight);
        }
    }
}

#[derive(Debug)]
pub struct IntoTestSecretRule;

impl Rule for IntoTestSecretRule {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_node(graph, ChainLinear)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: NodeIndex) {
        set_node(graph, node_index, TestSecret);
    }
}

#[derive(Debug)]
pub struct IntoKeyLockRule;

impl Rule for IntoKeyLockRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, ChainLinear, ChainLinear)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();
        set_node(graph, source, Key);
        set_node(graph, target, Lock);
    }
}

#[derive(Debug)]
pub struct IntoKeyCLLockRule;

impl Rule for IntoKeyCLLockRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, ChainLinear, ChainLinear)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();
        set_node(graph, source, Key);
        set_node(graph, target, Lock);
        graph.remove_edge(edge_index);
        let cl = graph.add_node(ChainLinear);
        graph.add_edge(source, cl, Edge::Tight);
        graph.add_edge(cl, target, Edge::Tight);
    }
}
