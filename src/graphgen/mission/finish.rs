use petgraph::prelude::*;

use super::{find_edge, set_node, MissionGraph, Rule, RuleSet};
use crate::graphgen::graph::Edge;
use crate::graphgen::mission::MissionNode::*;

pub fn all() -> RuleSet {
    let mut set = RuleSet::new("Finish");
    set.push_edge_rule(ChainRule);
    set
}

#[derive(Debug)]
pub struct ChainRule;

impl Rule for ChainRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<EdgeIndex> {
        find_edge(graph, ChainFinal, Goal)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);

        let gate_3 = graph.add_node(Gate);
        let lockfinal_4 = graph.add_node(LockFinal);
        let boss_5 = graph.add_node(Boss);
        let test_6 = graph.add_node(Test);
        let keyfinal_7 = graph.add_node(KeyFinal);
        let hook_8 = graph.add_node(Hook);
        let hook_9 = graph.add_node(Hook);

        set_node(graph, source, Chain);
        set_node(graph, target, Goal);

        graph.add_edge(source, hook_8, Edge::Normal);
        graph.add_edge(source, gate_3, Edge::Normal);
        graph.add_edge(source, test_6, Edge::Normal);
        graph.add_edge(test_6, keyfinal_7, Edge::Tight);
        graph.add_edge(test_6, hook_9, Edge::Normal);
        graph.add_edge(keyfinal_7, lockfinal_4, Edge::Normal);
        graph.add_edge(gate_3, lockfinal_4, Edge::Tight);
        graph.add_edge(lockfinal_4, boss_5, Edge::Tight);
        graph.add_edge(boss_5, target, Edge::Tight);
    }
}
