use petgraph::prelude::*;

use super::{find_edge, find_node, set_node, MissionGraph, Rule, RuleSet};
use crate::graphgen::{graph::Edge, mission::MissionNode::*};

pub fn all() -> RuleSet {
    let mut set = RuleSet::new("Parallel");
    set.push_edge_rule(CtoGRule);
    set.push_edge_rule(MultiKeyRule);
    set.push_edge_rule(ForkKLKmHRule);
    set.push_edge_rule(ForkKmToTestRule);
    set.push_edge_rule(ForkKmToTestSecretRule);
    set.push_edge_rule(ForkKToTestRule);
    set.push_edge_rule(ForkKToTestSecretRule);
    set.push_node_rule(ForkToHooks);
    set
}

#[derive(Debug)]
pub struct CtoGRule;

impl Rule for CtoGRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, Chain, Gate)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, _) = graph.edge_endpoints(edge_index).unwrap();
        set_node(graph, source, ChainParallel);
    }
}

#[derive(Debug)]
pub struct MultiKeyRule;

impl Rule for MultiKeyRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, ChainParallel, Gate)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);

        set_node(graph, source, Fork);
        set_node(graph, target, LockMulti);

        let k1 = graph.add_node(KeyMulti);
        let k2 = graph.add_node(KeyMulti);
        let k3 = graph.add_node(KeyMulti);

        graph.add_edge(source, k1, Edge::Normal);
        graph.add_edge(source, k2, Edge::Normal);
        graph.add_edge(source, k3, Edge::Normal);
        graph.add_edge(k1, target, Edge::Normal);
        graph.add_edge(k2, target, Edge::Normal);
        graph.add_edge(k3, target, Edge::Normal);
    }
}

#[derive(Debug)]
pub struct ForkKLKmHRule;

impl Rule for ForkKLKmHRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, Fork, KeyMulti)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);

        let key = graph.add_node(Key);
        let lock = graph.add_node(Lock);
        let hook = graph.add_node(Hook);

        graph.add_edge(source, key, Edge::Normal);
        graph.add_edge(key, lock, Edge::Normal);
        graph.add_edge(lock, hook, Edge::Tight);
        graph.add_edge(lock, target, Edge::Tight);
    }
}

#[derive(Debug)]
pub struct ForkKmToTestRule;

impl Rule for ForkKmToTestRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, Fork, KeyMulti)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);

        let test = graph.add_node(Test);

        graph.add_edge(source, test, Edge::Normal);
        graph.add_edge(test, target, Edge::Tight);
    }
}

#[derive(Debug)]
pub struct ForkKmToTestSecretRule;

impl Rule for ForkKmToTestSecretRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, Fork, KeyMulti)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);

        let test_secret = graph.add_node(TestSecret);

        graph.add_edge(source, test_secret, Edge::Normal);
        graph.add_edge(test_secret, target, Edge::Tight);
    }
}

#[derive(Debug)]
pub struct ForkKToTestRule;

impl Rule for ForkKToTestRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, Fork, Key)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);

        let test = graph.add_node(Test);

        graph.add_edge(source, test, Edge::Normal);
        graph.add_edge(test, target, Edge::Tight);
    }
}

#[derive(Debug)]
pub struct ForkKToTestSecretRule;

impl Rule for ForkKToTestSecretRule {
    type Index = EdgeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_edge(graph, Fork, Key)
    }

    fn apply(&self, graph: &mut MissionGraph, edge_index: EdgeIndex) {
        let (source, target) = graph.edge_endpoints(edge_index).unwrap();

        graph.remove_edge(edge_index);

        let test_secret = graph.add_node(TestSecret);

        graph.add_edge(source, test_secret, Edge::Normal);
        graph.add_edge(test_secret, target, Edge::Tight);
    }
}

#[derive(Debug)]
pub struct ForkToHooks;

impl Rule for ForkToHooks {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_node(graph, Fork)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: Self::Index) {
        set_node(graph, node_index, Nothing);

        let h1 = graph.add_node(Hook);
        let h2 = graph.add_node(Hook);

        for (node_index, edge_index) in graph
            .edges_directed(node_index, Outgoing)
            .map(|edge_ref| (edge_ref.target(), edge_ref.id()))
            .collect::<Vec<(NodeIndex, EdgeIndex)>>()
        {
            graph.remove_edge(edge_index);
            graph.add_edge(h1, node_index, Edge::Normal);
            graph.add_edge(h2, node_index, Edge::Normal);
        }

        graph.add_edge(node_index, h1, Edge::Normal);
        graph.add_edge(node_index, h2, Edge::Normal);
    }
}
