pub mod finish;
pub mod hook;
pub mod linear;
pub mod parallel;
pub mod start;

use petgraph::{prelude::*, stable_graph::GraphIndex};
use rand::{
    seq::{IteratorRandom, SliceRandom},
    RngCore,
};

use crate::graphgen::graph::{Edge, MissionNode};

pub type MissionGraph = DiGraph<MissionNode, Edge>;

pub fn new_mission<R>(rng: &mut R, rules: Vec<RuleSet>) -> MissionGraph
where
    R: RngCore + 'static,
{
    let mut graph = DiGraph::<_, _>::new();
    graph.add_node(MissionNode::Start);

    while !graph.node_weights().all(|weight| weight.terminal()) {
        let rule = rules.choose(rng).unwrap();
        rule.apply(&mut graph, rng);
    }

    graph
}

pub fn all_rulesets() -> Vec<RuleSet> {
    vec![
        start::all(),
        finish::all(),
        linear::all(),
        hook::all(),
        parallel::all(),
    ]
}

#[derive(Debug)]
enum RuleKind {
    Node(Box<dyn Rule<Index = NodeIndex>>),
    Edge(Box<dyn Rule<Index = EdgeIndex>>),
}

enum MatchKind {
    Node(NodeIndex),
    Edge(EdgeIndex),
}

pub trait Rule
where
    Self: std::fmt::Debug,
{
    type Index: GraphIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index>;

    fn apply(&self, graph: &mut MissionGraph, index: Self::Index);
}

pub struct RuleSet {
    name: String,
    rules: Vec<RuleKind>,
}

impl std::fmt::Debug for RuleSet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl RuleSet {
    pub fn new<S: Into<String>>(name: S) -> Self {
        Self {
            name: name.into(),
            rules: Default::default(),
        }
    }

    fn push_node_rule<R>(&mut self, rule: R)
    where
        R: Rule<Index = NodeIndex> + 'static,
    {
        self.rules.push(RuleKind::Node(Box::new(rule)));
    }

    fn push_edge_rule<R>(&mut self, rule: R)
    where
        R: Rule<Index = EdgeIndex> + 'static,
    {
        self.rules.push(RuleKind::Edge(Box::new(rule)));
    }

    pub fn apply<R>(&self, graph: &mut MissionGraph, rng: &mut R)
    where
        R: RngCore + 'static,
    {
        self.rules
            .iter()
            .choose_multiple(rng, self.rules.len())
            .iter()
            .find(|rule| match rule {
                RuleKind::Node(rule) => {
                    if let Some(index) = rule.find(&graph) {
                        rule.apply(graph, index);
                        true
                    } else {
                        false
                    }
                }
                RuleKind::Edge(rule) => {
                    if let Some(index) = rule.find(&graph) {
                        rule.apply(graph, index);
                        true
                    } else {
                        false
                    }
                }
            });
    }
}

/// Returns reference to the edge between a and b, if any.
fn find_edge(graph: &MissionGraph, a: MissionNode, b: MissionNode) -> Option<EdgeIndex> {
    let mut a_nodes = graph.node_indices().filter(|index| {
        graph
            .node_weight(*index)
            .and_then(|inner| Some(*inner == a))
            .unwrap_or_default()
    });

    a_nodes
        .find_map(|index| {
            graph.edges_directed(index, Outgoing).find(|edge| {
                graph
                    .node_weight(edge.target())
                    .and_then(|inner| Some(*inner == b))
                    .unwrap_or_default()
            })
        })
        .map(|edge_ref| edge_ref.id())
}

fn find_node(graph: &MissionGraph, node: MissionNode) -> Option<NodeIndex> {
    graph.node_indices().find(|index| {
        graph
            .node_weight(*index)
            .and_then(|inner| Some(*inner == node))
            .unwrap_or_default()
    })
}

fn set_node(graph: &mut MissionGraph, i: NodeIndex, node: MissionNode) {
    graph.node_weight_mut(i).map(|weight| *weight = node);
}
