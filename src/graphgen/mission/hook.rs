use petgraph::prelude::*;

use super::{find_node, set_node, MissionGraph, Rule, RuleSet};
use crate::graphgen::{graph::Edge, mission::MissionNode::*};

pub fn all() -> RuleSet {
    let mut set = RuleSet::new("Hook");
    set.push_node_rule(ToNothingRule);
    set.push_node_rule(ToItemBonusRule);
    set.push_node_rule(ToSecretItemBonusRule);
    set
}

#[derive(Debug)]
pub(crate) struct ToNothingRule;

impl Rule for ToNothingRule {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_node(graph, Hook)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: Self::Index) {
        set_node(graph, node_index, Nothing);
    }
}

#[derive(Debug)]
pub(crate) struct ToItemBonusRule;

impl Rule for ToItemBonusRule {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_node(graph, Hook)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: Self::Index) {
        set_node(graph, node_index, Test);

        // Remove edges all edges from source node
        for edge_index in graph
            .edges_directed(node_index, Outgoing)
            .map(|edge_ref| edge_ref.id())
            .collect::<Vec<EdgeIndex>>()
        {
            graph.remove_edge(edge_index);
        }

        let ib_node = graph.add_node(ItemBonus);
        graph.add_edge(node_index, ib_node, Edge::Tight);
    }
}

#[derive(Debug)]
pub(crate) struct ToSecretItemBonusRule;

impl Rule for ToSecretItemBonusRule {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<Self::Index> {
        find_node(graph, Hook)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: Self::Index) {
        set_node(graph, node_index, TestSecret);

        // Remove edges all edges from source node
        for edge_index in graph
            .edges_directed(node_index, Outgoing)
            .map(|edge_ref| edge_ref.id())
            .collect::<Vec<EdgeIndex>>()
        {
            graph.remove_edge(edge_index);
        }

        let ib_node = graph.add_node(ItemBonus);
        graph.add_edge(node_index, ib_node, Edge::Tight);
    }
}
