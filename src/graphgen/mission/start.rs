use petgraph::prelude::*;

use super::{find_node, set_node, MissionGraph, Rule, RuleSet};
use crate::graphgen::{graph::Edge, mission::MissionNode::*};

pub fn all() -> RuleSet {
    let mut set = RuleSet::new("Start");
    set.push_node_rule(StartRule);
    set
}

#[derive(Debug)]
pub struct StartRule;

impl Rule for StartRule {
    type Index = NodeIndex;

    fn find(&self, graph: &MissionGraph) -> Option<NodeIndex> {
        find_node(graph, Start)
    }

    fn apply(&self, graph: &mut MissionGraph, node_index: NodeIndex) {
        let chain = graph.add_node(Chain);
        let gate = graph.add_node(Gate);
        let miniboss = graph.add_node(MiniBoss);
        let quest_item = graph.add_node(QuestItem);
        let test_item = graph.add_node(TestItem);
        let chain_final = graph.add_node(ChainFinal);
        let exit = graph.add_node(Goal);

        set_node(graph, node_index, Entrance);
        graph.add_edge(node_index, chain, Edge::Normal);
        graph.add_edge(chain, gate, Edge::Normal);
        graph.add_edge(gate, miniboss, Edge::Tight);
        graph.add_edge(miniboss, quest_item, Edge::Tight);
        graph.add_edge(quest_item, test_item, Edge::Tight);
        graph.add_edge(test_item, chain_final, Edge::Tight);
        graph.add_edge(chain_final, exit, Edge::Tight);
    }
}
