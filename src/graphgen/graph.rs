use MissionNode::*;

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum MissionNode {
    MiniBoss,      // bm
    Boss,          // bl
    Chain,         // C
    ChainFinal,    // CF
    ChainLinear,   // CL
    ChainParallel, // CP
    Entrance,      // e
    Fork,          // F
    Gate,          // G
    Goal,          // g
    Hook,          // H
    ItemBonus,     // ib
    QuestItem,     // iq
    Key,           // k
    KeyFinal,      // kf
    KeyMulti,      // km
    Lock,          // l
    LockFinal,     // lf
    LockMulti,     // lm
    Nothing,       // n
    Start,         // S
    Test,          // t
    TestItem,      // ti
    TestSecret,    // ts
}

impl std::fmt::Debug for MissionNode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            MiniBoss => "MiniBoss",
            Boss => "Boss",
            Chain => "Chain",
            ChainFinal => "ChainFinal",
            ChainLinear => "ChainLinear",
            ChainParallel => "ChainParallel",
            Entrance => "Entrance",
            Fork => "Fork",
            Gate => "Gate",
            Goal => "Goal",
            Hook => "Hook",
            ItemBonus => "ItemBonus",
            QuestItem => "QuestItem",
            Key => "Key",
            KeyFinal => "KeyFinal",
            KeyMulti => "KeyMulti",
            Lock => "Lock",
            LockFinal => "LockFinal",
            LockMulti => "LockMulti",
            Nothing => "Nothing",
            Start => "Start",
            Test => "Test",
            TestItem => "TestItem",
            TestSecret => "TestSecret",
        };
        if self.terminal() {
            write!(f, "{}", s.to_lowercase())
        } else {
            write!(f, "{}", s.to_uppercase())
        }
    }
}

impl MissionNode {
    pub(crate) fn terminal(&self) -> bool {
        match self {
            Self::Boss
            | Self::Entrance
            | Self::Goal
            | Self::ItemBonus
            | Self::Key
            | Self::KeyFinal
            | Self::KeyMulti
            | Self::Lock
            | Self::LockFinal
            | Self::LockMulti
            | Self::MiniBoss
            | Self::Nothing
            | Self::QuestItem
            | Self::Test
            | Self::TestItem
            | Self::TestSecret => true,

            Self::Chain
            | Self::ChainLinear
            | Self::ChainFinal
            | Self::ChainParallel
            | Self::Fork
            | Self::Gate
            | Self::Hook
            | Self::Start => false,
        }
    }
}

#[derive(Copy, Clone)]
pub enum Edge {
    Normal,
    Tight,
}

impl std::fmt::Debug for Edge {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Edge::Normal => Ok(()),
            Edge::Tight => {
                write!(f, "T")
            }
        }
    }
}
