#![allow(clippy::type_complexity)]
#![allow(dead_code)]

mod abilities;
pub(crate) mod assets;

mod camera;
mod direction;
mod gamestate;
mod graphgen;
mod input;
mod player;
mod rng;
mod world;

use bevy::{
    app::AppExit,
    color::palettes,
    log::LogPlugin,
    pbr::wireframe::WireframePlugin,
    prelude::*,
    render::{
        settings::{RenderCreation, WgpuFeatures, WgpuSettings},
        RenderPlugin,
    },
};
use bevy_inspector_egui::quick::WorldInspectorPlugin;
#[cfg(debug_assertions)]
#[cfg(feature = "dbg_physics")]
use bevy_rapier3d::prelude::RapierDebugRenderPlugin;
use bevy_rapier3d::prelude::{NoUserData, RapierPhysicsPlugin};
#[cfg(debug_assertions)]
use color_eyre::eyre::Result;
use input::InputEvent;

struct CorePlugin;

impl Plugin for CorePlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(gamestate::GameStatePlugin)
            .add_plugins(rng::RngPlugin)
            .add_plugins(world::WorldPlugin)
            .add_plugins(input::InputPlugin)
            .add_plugins(abilities::AbilityPlugin)
            // .add_plugins(player::PlayerPlugin)
            .add_plugins(camera::CameraPlugin)
            .add_systems(Update, quit);
    }
}

struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(WireframePlugin);
        #[cfg(feature = "dbg_physics")]
        app.add_plugins(RapierDebugRenderPlugin::default());
        app.add_plugins(WorldInspectorPlugin::new());
        app.add_systems(Update, gizmos);
    }
}

fn main() -> Result<()> {
    #[cfg(debug_assertions)]
    color_eyre::install()?;

    let mut app = App::new();
    app.add_plugins(
        DefaultPlugins
            .set(ImagePlugin::default_nearest())
            .set(LogPlugin {
                filter: "error,procedural=debug".into(),
                ..default()
            })
            .set(RenderPlugin {
                render_creation: RenderCreation::Automatic(WgpuSettings {
                    features: WgpuFeatures::POLYGON_MODE_LINE,
                    ..default()
                }),
                ..default()
            }),
    )
    .add_plugins(RapierPhysicsPlugin::<NoUserData>::default());
    app.add_plugins(CorePlugin);
    #[cfg(debug_assertions)]
    app.add_plugins(DebugPlugin);
    app.run();

    Ok(())
}

fn quit(mut gameinput: EventReader<InputEvent>, mut app_exit_events: ResMut<Events<AppExit>>) {
    if gameinput
        .read()
        .any(|event| matches!(event, InputEvent::Quit))
    {
        app_exit_events.send(AppExit::Success);
    }
}

fn gizmos(mut gizmos: Gizmos) {
    gizmos.ray(Vec3::ZERO, Vec3::X * 1000., palettes::basic::RED);
    gizmos.ray(Vec3::ZERO, Vec3::Z * 1000., palettes::basic::BLUE);
}
